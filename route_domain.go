package bird

import (
	"strings"
)

//
type routeDomain struct {
}

//
var _routeDomain *routeDomain

//
func RouteDomain() *routeDomain {
	if _routeDomain == nil {
		object := new(routeDomain)
		_routeDomain = object
	}
	return _routeDomain
}

//
type RouteInfo struct {
	Method string `json:"method"`
	Path   string `json:"path"`
}

//路由列表
//mothod 匹配查询
//route 模糊查询
func (this *routeDomain) Select(method string, path string) []*RouteInfo {
	routes := CoreDomain().server.Routes()
	list := make([]*RouteInfo, 0)
	for _, r := range routes {
		if path != "" && strings.Index(r.Path, path) < 0 {
			continue
		}
		if method != "" && r.Method != strings.ToUpper(method) {
			continue
		}
		list = append(list, &RouteInfo{
			Method: r.Method,
			Path:   r.Path,
		})
	}
	return list
}
