package bird

import (
	"time"

	"gitee.com/web-bird/bird/try"
	"github.com/gin-gonic/gin"

	"gitee.com/web-bird/bird/middleware"
)

//jwt处理器，本文件的JWT结构用于应用层直接继承使用
type JWT struct {
	middleware.JWT
}

//发行者
func (this *JWT) Issuer() string {
	return config.JWT.Issuer
}

//设置密匙
func (this *JWT) Secret() string {
	return config.JWT.Secret
}

//登录时间
func (this *JWT) Duration() time.Duration {
	return time.Minute * time.Duration(config.JWT.Duration)
}

//
func (this *JWT) Error(*gin.Context) {
	try.Throw(&ExceptionJWT{}, "登录异常")
}
