package middleware

import (
	"github.com/gin-gonic/gin"
)

//限制最大允许连接数的中间件,最低255并发
func MaxAllowed(n int) gin.HandlerFunc {
	if n == 0 {
		n = 255
	}
	type s struct{}
	sem := make(chan s, n)
	acquire := func() {
		sem <- s{}
	}
	release := func() {
		<-sem
	}
	return func(c *gin.Context) {
		acquire()       //before request
		defer release() //after request
		c.Next()
	}
}
