package middleware

import (
	"fmt"
	"gitee.com/web-bird/bird/assets/utils"
	"sync"

	"github.com/gin-gonic/gin"
)

//签名验证中间件
//签名长度在74-104字符之间
//signKey=sign的header key名
//secret=密匙
var signsMap = new(sync.Map)
var signLength = 0

func Sign(signKey, secret string) gin.HandlerFunc { //
	return func(c *gin.Context) {
		signLength++
		if signLength > 100000 {
			signsMap = new(sync.Map)
			signLength = 0
		}
		signStr := c.GetHeader(signKey)
		if l := len(signStr); l < 74 || l > 104 {
			c.AbortWithStatus(403)
			return
		}
		signIndex := utils.Sha256(c.ClientIP(), c.Request.RequestURI, signStr)
		if _, ok := signsMap.Load(signIndex); ok {
			c.AbortWithStatus(403)
			return
		}
		start := len(signStr) - 64
		token := signStr[:start]
		sign := signStr[start:]
		if utils.Sha256(fmt.Sprint(token, secret)) != sign { //签名未通过
			c.AbortWithStatus(403)
			return
		}
		signsMap.Store(signIndex, true)
	}
}
