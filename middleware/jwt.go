package middleware

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

/*
type Handler struct {
}
//发行者
func (this *Handler) Issuer() string {
	return "test"
}

//设置密匙
func (this *Handler) Secret() string {
	return "abcdefaasdfasdft"
}

//获取token的方式
func (this *Handler) Token(w *Wontext) string {
	var token = ""
	if w.Request.Header["Authorization"] != nil {
		token = w.Request.Header["Authorization"][0]
	}
	return token
}

//当错误时
func (this *Handler) Error(w *Wontext) {
	token, e := w.GenerateToken("hait", "hait", 323)
	fmt.Println("jwt 加密结果：", token, e)
	w.Succ(100, token)
}

//当超时时
func (this *Handler) Timeout(w *Wontext) {

}
*/
const (
	GIN_KEY_JWT = "gin_key_jwt"
)

//jwt数据保存结构
type Claims struct {
	UserId   int64  `json:"userid"`
	Username string `json:"username"`
	Outtime  int64  `json:"outTime"`
	Group    string `json:"group"`
	jwt.StandardClaims
}

//jwt配置接口
type JWTConfigInterface interface {
	Group() string             //分组
	Issuer() string            //发行人
	Secret() string            //密匙
	Token(*gin.Context) string //口令获取方式
	Error(*gin.Context)        //当出现错误时
	Timeout(*gin.Context)      //当超时
	Duration() time.Duration   //生存时间(单位分）
}

//jwt处理结构，继承使用
type JWT struct {
	conf JWTConfigInterface
}

//
func (this *JWT) Init(conf JWTConfigInterface) {
	this.conf = conf
}

//拦截中间件
func (this *JWT) Handler() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := this.conf.Token(c)
		//token为空时
		if token == "" {
			this.conf.Error(c)
			c.Abort()
			return
		}
		claims, err := this.ParseToken(token)
		//token错误时
		if err != nil {
			this.conf.Error(c)
			c.Abort()
			return
		}
		//分组不正确时
		if claims.Group != this.conf.Group() {
			this.conf.Error(c)
			c.Abort()
			return
		}
		var now = time.Now().Unix()
		//当超时时
		if now > claims.Outtime && now < claims.ExpiresAt {
			this.conf.Timeout(c)
			c.Abort()
			return
		}
		c.Set(GIN_KEY_JWT, claims)
	}
}

//生成token
//username 用户名
//id 用户id
//group 用户组
func (this *JWT) GenerateToken(id int64, username string) (string, error) {
	nowTime := time.Now()
	expireTime := nowTime.Add(this.conf.Duration())  //失效时间
	outTime := nowTime.Add(this.conf.Duration() / 2) //刷新token时间
	claims := Claims{
		UserId:   id,
		Username: username,
		Outtime:  outTime.Unix(),
		Group:    this.conf.Group(),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    this.conf.Issuer(),
		},
	}
	//crypto.Hash加密方案
	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//该方法内部生成签名字符串，再用于获取完整、已签名的token
	token, err := tokenClaims.SignedString([]byte(this.conf.Secret()))
	if err != nil {
		return "", err
	}
	return token, nil
}

//解析
func (this *JWT) ParseToken(token string) (*Claims, error) {
	//用于解析鉴权的声明，方法内部主要是具体的解码和校验的过程，最终返回*Token
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(this.conf.Secret()), nil
	})
	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	return nil, err
}
