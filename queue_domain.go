package bird

import (
	"fmt"
	"log"
	"sync"
	"time"

	"gitee.com/web-bird/bird/try"
)

type queueDomain struct {
	running bool
	queues  map[string]QueueInterface
	sync.Mutex
}

//
var _queueDomain *queueDomain

//
func QueueDomain() *queueDomain {
	if _queueDomain == nil {
		object := new(queueDomain)
		object.queues = make(map[string]QueueInterface)
		_queueDomain = object
	}
	return _queueDomain
}

func (this *queueDomain) GetName(alias string) string {
	this.Lock()
	defer this.Unlock()
	if queue, ok := this.queues[alias]; ok {
		return queue.Name()
	}
	return ""
}

//加载
func (this *queueDomain) load(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, queue := range mod.Queues() {
		if _, ok := this.queues[queue.Alias()]; ok {
			try.ThrowFatal("队列【" + queue.Name() + "】对象名重复")
			return
		}
		this.queues[queue.Alias()] = queue
	}
	this.run()
}

func (this *queueDomain) unload(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, queue := range mod.Queues() {
		delete(this.queues, queue.Alias())
	}
}

func (this *queueDomain) run() {
	if this.running {
		return
	}
	this.running = true
	try.Go(func() {
		time.Sleep(time.Second * 60)
		if taskStore == nil || config.CloseQueue {
			return
		}
		for {
			//调用了外部方法，采用不信任方式
			try.Do(func() {
				this.handleQueue()
			}, func(e try.Exception) {
				log.Println("队列处理异常：", e.Name(), e.GetMsg(), e.String())
				time.Sleep(time.Second * 60) //对列异常，停顿60秒再处理
				//此处应有钩子通知
			})
		}
	})
}

var queueLock = sync.Mutex{}

//队列消费
func (this *queueDomain) handleQueue() {
	queueLock.Lock()
	defer queueLock.Unlock()
	b := NewBird().setClass(BIRD_CLASS_QUEUE)
	task := taskStore(b).TaskGet()
	if task == nil {
		time.Sleep(time.Second * 5)
		return
	}
	done := make(chan bool)
	queue, ok := this.queues[task.QueueAlias]
	if !ok {
		time.Sleep(time.Second * 5)
		return
	}
	b.setName(queue.Name())
	try.Go(func() {
		b.Transaction(func() {
			queue.Handler(b, task)
			taskStore(b).TaskDone(b, task) //任务完成
		})
		done <- true
	}, func(e try.Exception) {
		task.Err = fmt.Sprint(e.GetMsg(), e.GetData())
		taskStore(b).TaskErr(b, task)
		//ExceptionDomain().handler(b, e)
		done <- false
	})
	select {
	case d := <-done:
		if !d {
			time.Sleep(time.Second * 60) //对列异常，停顿60秒再处理
		}
	case <-time.After(time.Second * 60): //60秒未完成则任务超时
		try.ThrowFatal("队列执行超时")
	}
}

//查询结构
type QueueInfo struct {
	ModuleName string `json:"moduleName"`
	Alias      string `json:"alias"` //队列别名
	Name       string `json:"name"`  //名称
	queue      QueueInterface
}

//查询配置,可根据模块查询
func (this *queueDomain) Select(moduleName string) []*QueueInfo {
	list := make([]*QueueInfo, 0)
	for _, mod := range ModuleDomain().modules {
		if moduleName != "" && mod.ModuleName() != moduleName {
			continue
		}
		for _, queue := range mod.Queues() {
			list = append(list, &QueueInfo{
				Name:       queue.Name(),
				Alias:      queue.Alias(),
				ModuleName: mod.ModuleName(),
			})
		}
	}
	return list
}
