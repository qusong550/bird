package bird

import (
	"fmt"
	"gitee.com/web-bird/bird/try"
	"sync"
)

//
type permissionDomain struct {
	pool  map[string]*Permission
	index []string
	sync.Mutex
}

var _permissionDomain *permissionDomain

//权限处理域
func PermissionDomain() *permissionDomain {
	if _permissionDomain == nil {
		object := new(permissionDomain)
		object.pool = make(map[string]*Permission)
		object.index = make([]string, 0)
		_permissionDomain = object
	}
	return _permissionDomain
}

//
func (this *permissionDomain) New(id, name string) *Permission {
	this.Lock()
	defer this.Unlock()
	permission := this.new(id, name, "")
	this.pool[id] = permission
	this.index = append(this.index, id)
	return permission
}

//
func (this *permissionDomain) new(id, name, pid string) *Permission {
	if _, ok := this.pool[id]; ok {
		try.ThrowFatal(fmt.Sprintf("权限编号【%s】重复", id))
	}
	permission := &Permission{Id: id, Name: name, Pid: pid}
	permission.Childs = make([]*Permission, 0)
	return permission
}

//查询所有权限
//permissionId =权限id模糊查询
func (this *permissionDomain) Select() []*Permission {
	list := make([]*Permission, 0)
	for _, id := range this.index {
		p, ok := this.pool[id]
		if !ok {
			continue
		}
		list = append(list, &Permission{
			Id:     p.Id,
			Pid:    p.Pid,
			Name:   p.Name,
			Childs: this.getChilds(p),
		})
	}
	return list
}

func (this *permissionDomain) getChilds(p *Permission) []*Permission {
	list := make([]*Permission, 0)
	if len(p.Childs) == 0 {
		return list
	}
	for _, p := range p.Childs {
		list = append(list, &Permission{
			Id:     p.Id,
			Pid:    p.Pid,
			Name:   p.Name,
			Childs: this.getChilds(p),
		})
	}
	return list
}
