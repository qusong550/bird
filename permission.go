package bird

import (
	"gitee.com/web-bird/bird/try"
	"github.com/gin-gonic/gin"
)

//权限
type Permission struct {
	Id     string        `json:"id"`     //权限id
	Pid    string        `json:"pid"`    //权限上级id
	Name   string        `json:"name"`   //权限名
	Childs []*Permission `json:"childs"` //子权限
}

//生成子权限
//id不能重复
func (this *Permission) Child(id string, name string) *Permission {
	object := PermissionDomain().new(id, name, this.Id)
	this.Childs = append(this.Childs, object)
	return object
}

//监视并拦截,fn可以为nil
func (this *Permission) Watch(fn gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		if !CoreDomain().app.PermissionHandler(c, this) {
			try.Throw(&ExceptionPermission{}, "没有权限")
		}
		if fn != nil {
			fn(c)
		}
	}
}
