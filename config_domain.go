package bird

import (
	"fmt"
	"sync"
	"time"

	"gitee.com/web-bird/bird/assets/utils"

	pconfig "gitee.com/web-bird/bird/assets/config"

	"gitee.com/web-bird/bird/try"
)

//查询结构
type ConfigInfo struct {
	ModuleName string          `json:"moduleName"` //模块名
	Name       string          `json:"name"`       //配置名称
	Alias      string          `json:"alias"`      //配置别名
	Filename   string          `json:"file"`       //文件
	config     ConfigInterface //配置对象
}

type configDomain struct {
	pool     map[string]*ConfigInfo
	configs  map[string]ConfigInterface
	defaults map[string]ConfigInterface
	index    []string //序列
	rlock    sync.Mutex
	sync.Mutex
}

//
var _configDomain *configDomain

//配置处理域
func ConfigDomain() *configDomain {
	if _configDomain == nil {
		object := new(configDomain)
		object.pool = make(map[string]*ConfigInfo)
		object.defaults = make(map[string]ConfigInterface)
		object.configs = make(map[string]ConfigInterface)
		object.index = make([]string, 0)
		object.rlock = sync.Mutex{}
		_configDomain = object
	}
	return _configDomain
}

func (this *configDomain) load(mod ModuleInterface) {
	this.rlock.Lock()
	defer this.rlock.Unlock()
	for _, component := range mod.Components() {
		configInterface, ok := component.(Configer)
		if !ok {
			continue
		}
		conf := configInterface.Config()
		var alias = conf.Alias()
		if _, ok := this.pool[alias]; ok {
			try.ThrowFatal(fmt.Sprintf("配置%s重复", conf.Name()))
		}
		this.pool[alias] = &ConfigInfo{
			ModuleName: mod.ModuleName(),
			Name:       conf.Name(),
			Alias:      alias,
			Filename:   this.configFilename(alias),
			config:     conf,
		}
		this.index = append(this.index, alias)
		config := this.Get(conf)
		this.SaveByPtr(config) //更新配置
	}
}

func (this *configDomain) unload(mod ModuleInterface) {
	this.rlock.Lock()
	defer this.rlock.Unlock()
	deleteIndexs := make(map[string]bool)
	for _, component := range mod.Components() {
		conf, ok := component.(Configer)
		if !ok {
			continue
		}
		var alias = conf.Config().Alias()
		deleteIndexs[alias] = true
		delete(this.pool, alias)
	}
	index := make([]string, 0)
	for _, alias := range this.index {
		if !deleteIndexs[alias] {
			index = append(index, alias)
		}
	}
	this.index = index
}

//
func (this *configDomain) loadCoreConfig() {
	coreConfigFilename := this.configFilename("core")
	{ //默认配置
		config.Debug = false
		config.MD5 = utils.Md5(time.Now().Unix())
		config.ServerAddr = ":8803"
		config.Database.Type = "mysql"
		config.Database.Source = "用户名:密码@tcp(127.0.0.1:3306)/数据库?charset=utf8mb4"
		config.JWT.Duration = 60 * 24 * 3
		config.JWT.Issuer = "bird"
		config.JWT.Secret = utils.Sha256(time.Now())
	}
	//加载系统配置
	if err := pconfig.Read(coreConfigFilename, &config); err != nil {
		try.ThrowFatal("核心配置读取失败", err.Error())
	}
	if err := pconfig.Write(coreConfigFilename, config); err != nil {
		try.ThrowFatal("系统配置文件初始化失败", err.Error())
	}
}

//查询配置,可根据模块查询
func (this *configDomain) Select(moduleName string) []*ConfigInfo {
	list := make([]*ConfigInfo, 0)
	for _, name := range this.index {
		conf, ok := this.pool[name]
		if !ok {
			continue
		}
		if moduleName != "" && conf.ModuleName != moduleName {
			continue
		}
		list = append(list, &ConfigInfo{
			Name:       conf.Name,
			Alias:      conf.Alias,
			Filename:   conf.Alias,
			ModuleName: conf.ModuleName,
		})
	}
	return list
}

//获取配置,如果配置不存在则返回默认配置
//该方法返回一个副本，避免外部修改了配置
func (this *configDomain) Get(config ConfigInterface) ConfigInterface {
	this.Lock()
	defer this.Unlock()
	alias := config.Alias()
	conf, ok := this.configs[alias]
	if !ok {
		this.defaults[config.Alias()] = utils.DeepClone(config).(ConfigInterface)
		conf = config.Default()
		filename := this.configFilename(alias)
		if err := pconfig.Read(filename, conf); err != nil {
			try.ThrowWarn("配置读取失败", filename, err.Error())
		}
		this.configs[alias] = conf
	}
	return utils.DeepClone(conf).(ConfigInterface)
}

//获取默认配置
func (this *configDomain) GetByDefault(alias string) ConfigInterface {
	this.Lock()
	defer this.Unlock()
	conf, ok := this.defaults[alias]
	if !ok {
		try.ThrowFatalf("配置获取失败", "配置：%s", alias)
	}
	return utils.DeepClone(conf).(ConfigInterface)
}

//根据别名获取配置
func (this *configDomain) GetByAlias(alias string) ConfigInterface {
	this.Lock()
	defer this.Unlock()
	conf, ok := this.configs[alias]
	if !ok {
		try.ThrowFatalf("配置获取失败", "配置：%s", alias)
	}
	return utils.DeepClone(conf).(ConfigInterface)
}

//保存配置
func (this *configDomain) SaveByPtr(confPtr ConfigInterface) {
	this.Lock()
	defer this.Unlock()
	alias := confPtr.Alias()
	filename := this.configFilename(alias)
	if err := pconfig.Write(filename, confPtr); err != nil {
		try.ThrowFatalf("配置保存失败", "文件名：%s，错误：%s", filename, err.Error())
	}
	if conf, ok := this.pool[alias]; ok {
		conf.config = confPtr
	}
	this.configs[alias] = confPtr
}

//获取配置信息
//tag标记内容：<name>标题，<desc>说明，<options>选项
//返回=配置结构,配置
func (this *configDomain) GetInfo(configAlias string) ([]*pconfig.ConfigStruct, ConfigInterface) {
	conf := this.GetByAlias(configAlias)
	return pconfig.Dump(conf), conf
}

//根据别名获取配置文件名
func (this *configDomain) configFilename(alias string) string {
	return fmt.Sprintf("%s/%s.bird.json", CONFIG_DIR, alias)
}
