package bird

import (
	"fmt"
	"gitee.com/web-bird/bird/try"
	"sync"
)

type hookDomain struct {
	hooks  map[string]*HookInfo
	events map[string][]*EventInfo
	sync.Mutex
}

var _hookDomain *hookDomain

//
func HookDomain() *hookDomain {
	if _hookDomain == nil {
		object := new(hookDomain)
		object.hooks = make(map[string]*HookInfo)
		object.events = make(map[string][]*EventInfo)
		_hookDomain = object
	}
	return _hookDomain
}

func (this *hookDomain) GetHookname(hookId string) string {
	this.Lock()
	defer this.Unlock()
	if hook, ok := this.hooks[hookId]; ok {
		return hook.Name
	}
	return ""
}

func (this *hookDomain) load(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, hook := range mod.Hooks() {
		if _, ok := this.hooks[hook.Id]; ok {
			try.ThrowFatal(fmt.Sprintf("钩子【%s-%s】重复", hook.Name, hook.Id))
		}
		var hookModeName = "同步"
		if hook.Mode == HOOK_ASYNC {
			hookModeName = "异步"
		}
		this.hooks[hook.Id] = &HookInfo{
			HookId:     hook.Id,
			Name:       hook.Name,
			ModuleName: mod.ModuleName(),
			Mode:       hook.Mode,
			ModeName:   hookModeName,
			ArgsDesc:   hook.ArgsDesc,
			Events:     make([]*EventInfo, 0),
		}
	}
	for _, event := range mod.Events() {
		hookId := event.HookId()
		if _, ok := this.events[hookId]; !ok {
			this.events[hookId] = make([]*EventInfo, 0)
		}
		this.events[hookId] = append(this.events[hookId], &EventInfo{
			HookId:     event.HookId(),
			ModuleName: mod.ModuleName(),
			event:      event,
		})
	}
}

func (this *hookDomain) unload(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, hook := range mod.Hooks() {
		delete(this.hooks, hook.Id)
	}
	for hookId, arr := range this.events {
		events := make([]*EventInfo, 0)
		for _, event := range arr {
			if event.ModuleName == mod.ModuleName() {
				continue
			}
			events = append(events, event)
		}
		this.events[hookId] = events
	}
}

func (this *hookDomain) trigger(hookId string, b *Bird, args ...interface{}) {
	hook, ok := this.hooks[hookId]
	if !ok {
		return
	}
	events, ok := this.events[hookId]
	if !ok {
		return
	}
	if b == nil {
		b = NewBird().setClass(BIRD_CLASS_EVENT).setName(hook.Name)
	}
	if hook.Mode == HOOK_ASYNC {
		try.Go(func() {
			for _, event := range events {
				event.event.Handler(b, args...)
			}
		}, func(e try.Exception) {
			ExceptionDomain().handler(b, e)
		})
	} else {
		for _, event := range events {
			event.event.Handler(b, args...)
		}
	}
}

//查询结构
type HookInfo struct {
	HookId     string       `json:"hookId"`     //编号
	Name       string       `json:"name"`       //名称
	ModuleName string       `json:"moduleName"` //所属模块名
	Mode       int          `json:"mode"`       //钩子类型
	ModeName   string       `json:"modeName"`   //钩子类型
	ArgsDesc   string       `json:"argsDesc"`   //参数说明
	Events     []*EventInfo `json:"events"`     //已经注册的事件
}

//钩子列表
func (this *hookDomain) SelectHook(moduleName string, hookId string) []*HookInfo {
	list := make([]*HookInfo, 0)
	for _, hook := range this.hooks {
		if hookId != "" && hook.HookId != hookId {
			continue
		}
		if moduleName != "" && hook.ModuleName != moduleName {
			continue
		}
		events := this.events[hook.HookId]
		list = append(list, &HookInfo{
			Name:       hook.Name,
			HookId:     hook.HookId,
			ModuleName: hook.ModuleName,
			Mode:       hook.Mode,
			ArgsDesc:   hook.ArgsDesc,
			Events:     events,
		})
	}
	return list
}

//查询结构
type EventInfo struct {
	HookId     string `json:"hookId"`
	Name       string `json:"name"` //事件名称
	ModuleName string `json:"moduleName"`
	event      EventInterface
}

//已注册事件列表
func (this *hookDomain) SelectEvent(moduleName string, hookId string) []*EventInfo {
	list := make([]*EventInfo, 0)
	for _, hook := range this.hooks {
		if hookId != "" && hook.HookId != hookId {
			continue
		}
		events, ok := this.events[hook.HookId]
		if !ok {
			continue
		}
		for _, event := range events {
			if moduleName != "" && event.ModuleName != moduleName {
				continue
			}
			list = append(list, &EventInfo{
				HookId:     event.HookId,
				Name:       hook.Name,
				ModuleName: event.ModuleName,
			})
		}
	}
	return list
}
