package bird

import (
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/web-bird/bird/assets/orm"
)

//划分鸟类
const (
	BIRD_CLASS_FREE    = 1 //系统(自由鸟）
	BIRD_CLASS_ROUTER  = 2 //路由
	BIRD_CLASS_QUEUE   = 3 //队列
	BIRD_CLASS_CRONTAB = 4 //定时任务
	BIRD_CLASS_EVENT   = 5 //钩子
)

//鸟上下文，所有控制器部分都贯穿着这只鸟，建议所有模型、组件都继承这只鸟
//可以更方便实现事务的同步
type Bird struct {
	orm.Engine          //集成orm引擎
	Context    *Context //请求上下文（如果不是http请求则为nil)
	birdId     int64    //当前id
	birdIP     string   //鸟ip
	birdClass  int      //鸟类型（
	birdName   string   //鸟id（控制器=路由，队列=名称，任务=名称，钩子=名称）
	handler    string   //操作人
	singleMap  map[string]interface{}
	sync.Mutex
}

//自增id
var birdIndex = time.Now().Unix()

//创建一只鸟,方便单独使用
func NewBird() *Bird {
	bird := new(Bird)
	bird.birdClass = BIRD_CLASS_FREE
	bird.birdName = "free"
	atomic.AddInt64(&birdIndex, 1)
	bird.birdId = birdIndex
	bird.singleMap = make(map[string]interface{})
	return bird
}

//设置鸟类
func (this *Bird) setClass(class int) *Bird {
	this.birdClass = class
	return this
}

//设置鸟名
func (this *Bird) setName(name string) *Bird {
	this.birdName = name
	return this
}

//设置操作人
func (this *Bird) SetHandler(name string) *Bird {
	this.handler = name
	return this
}

//初始化配置，并自动获取配置(为避免程序运行中修改配置，获取的对象值为克隆值)
func (this *Bird) ConfigInit(confPtr ConfigInterface) ConfigInterface {
	return ConfigDomain().Get(confPtr)
}

//单例对象
func (this *Bird) Single(name string, createFunc func() interface{}) interface{} {
	this.Lock()
	defer this.Unlock()
	object, ok := this.singleMap[name]
	if !ok {
		object = createFunc()
		this.singleMap[name] = object
	}
	return object
}

//获取鸟类名
func GetBirdClassName(classId int, name string) (string, string) {
	var class string
	switch classId {
	case BIRD_CLASS_FREE:
		class = "未定义"
	case BIRD_CLASS_ROUTER:
		class = "路由"
	case BIRD_CLASS_QUEUE:
		class = "队列"
	case BIRD_CLASS_CRONTAB:
		class = "定时任务"
	case BIRD_CLASS_EVENT:
		class = "钩子"
		name = HookDomain().GetHookname(name)
	}
	return class, name
}

//
