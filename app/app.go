package app

import (
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/try"
	"gitee.com/web-bird/bird/wing"
	"log"
)

//
type coreApp struct {
	bird.AppInterface
}

//加载wing模块
func (this *coreApp) Modules() []bird.ModuleInterface {
	modules := []bird.ModuleInterface{wing.New()}
	return append(modules, this.AppInterface.Modules()...)
}

func (this *coreApp) FromBird() {

}

//启动应用
func Run(app bird.AppInterface) {
	try.Do(func() {
		bird.CoreDomain().Start(&coreApp{app})
	}, func(e try.Exception) {
		log.Println("程序启动失败：", e.GetMsg(), e.GetData())
		if msg := e.String(); msg != "" {
			//log.Println("启动失败原因：", msg, string(debug.Stack()))
		}
	})
}
