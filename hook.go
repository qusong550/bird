package bird

/*
	事件钩子
*/

const (
	HOOK_SYNC  = 0 //同步（默认）
	HOOK_ASYNC = 1 //异步
)

//钩子
type Hook struct {
	Id       string `json:"id"`   //钩子id
	Name     string `json:"name"` //钩子名称
	Mode     int    `json:"mode"` //钩子类型（1=同步，2=异步）
	ArgsDesc string `json:"desc"` //参数说明
}

//放置钩子,如果是异步钩子则开票协程执行
func (this *Hook) Put(args ...interface{}) {
	HookDomain().trigger(this.Id, nil, args...)
}

func (this *Hook) PutOnBird(b *Bird, args ...interface{}) {
	HookDomain().trigger(this.Id, b, args...)
}
