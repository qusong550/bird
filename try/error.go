package try

import (
	"fmt"
)

// 运行时异常-------------------------
type ExceptionRuntime struct {
	ExceptionBase
}

func (this *ExceptionRuntime) Name() string {
	return "运行"
}

// 提示异常-------------------------
type ExceptionInfo struct {
	ExceptionBase
}

// --
func (this *ExceptionInfo) Name() string {
	return "信息"
}

// 抛出信息级别错误
func ThrowInfo(title string, errs ...interface{}) {
	Throw(&ExceptionInfo{}, title, fmt.Sprint(errs...))
}

// 抛出信息级别错误
func ThrowInfof(title string, format string, errs ...interface{}) {
	Throw(&ExceptionInfo{}, title, fmt.Sprintf(format, errs...))

}

// 警告级别错误（可输出到前台）--------------
type ExceptionWarn struct {
	ExceptionBase
}

func (this *ExceptionWarn) Name() string {
	return "警告"
}

// 抛出警告级别错误
func ThrowWarn(title string, errs ...interface{}) {
	Throw(&ExceptionWarn{}, title, fmt.Sprint(errs...))
}

// 抛出警告级别错误
func ThrowWarnf(title string, format string, errs ...interface{}) {
	Throw(&ExceptionWarn{}, title, fmt.Sprintf(format, errs...))

}

// 致命级别错误（不可输出到前台）-----------
type ExceptionFatal struct {
	ExceptionBase
}

func (this *ExceptionFatal) Name() string {
	return "致命"
}

// 抛出致命级别错误
func ThrowFatal(title string, errs ...interface{}) {
	Throw(&ExceptionFatal{}, title, fmt.Sprint(errs...))
}

// 抛出致命级别错误
func ThrowFatalf(title string, format string, errs ...interface{}) {
	Throw(&ExceptionFatal{}, title, fmt.Sprintf(format, errs...))
}
