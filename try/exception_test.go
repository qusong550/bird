package try

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"
)

type ExceptionDatabase struct {
	ExceptionBase
}

type b struct {
}

func (this *b) p() {
	panic("协程抛出异常")
}

type per interface {
	p()
}

//
func aTestThread(t *testing.T) {
	c := new(b)
	var b per
	b = c
	Go(func() {
		b.p()
	}, func(e Exception) {
		fmt.Println("捕捉：", e.String())
		fmt.Println("===========================")

		panic("asdf")
	})
	time.Sleep(time.Second * 1)
}

func aTestException(t *testing.T) {
	SetExceptionHandler(func(e Exception) {
		fmt.Println("这是怎么回事：", e)
	})
	Do(func() {
		Go(func() {
			time.Sleep(time.Second * 1)
		}, func(e Exception) {
			fmt.Println("纤程收到异常:", e, reflect.TypeOf(e).Elem().Name())
			panic("哇靠")
		})
		Throw(&ExceptionFatal{}, "是是是")
	}, func(e Exception) {
		switch t := e.(type) {
		case *ExceptionFatal:
			println("异常成功---：", t.GetMsg())
		}
		fmt.Println("收到异常:", e.GetMsg())
		fmt.Println("异常类型:", reflect.TypeOf(e).Elem().Name())
		//for i, v := range strings.Split(e.Stack(), "\n") {
		//fmt.Println(i, strings.TrimSpace(v))
		//}
	})
	time.Sleep(time.Second * 3)
}

//
const testNumber = 2000000

//err逻辑性能测试//2000000000
func Benchmark_errTest(b *testing.B) {
	for i := 0; i < testNumber; i++ { //use b.N for looping
		func() {
			var err error
			defer func() {
				if err != nil {
					return
				}
			}()
			err = makeError(0)
		}()
	}
}

//panic逻辑性能测试
func Benchmark_panicTest(b *testing.B) {
	for i := 0; i < testNumber; i++ { //use b.N for looping
		func() {
			defer func() {
				if err := recover(); err != nil {
					return
				}
			}()
			makePanic(0)
		}()
	}
}

func makeError(i int) error {
	if i == 30 {
		return errors.New("哈哈哈哈哈哈哈哈哈哈哈哈哈哈")
	}
	i++
	if err := makeError(i); err != nil {
		return err
	}
	return nil
}

func makePanic(i int) {
	if i == 30 {
		panic(errors.New("哈哈哈哈哈哈哈哈哈哈哈哈哈哈"))
	}
	i++
	makeError(i)
}
