package try

import (
	"fmt"
	"runtime/debug"
)

//默认异常
type ExceptionBase struct {
	msg  string
	data interface{}
}

//创建异常
func (this *ExceptionBase) Init(msg string, data interface{}) {
	this.msg = msg
	this.data = data
}

func (this *ExceptionBase) Name() string {
	return "默认"
}

//标题
func (this *ExceptionBase) GetMsg() string {
	return this.msg
}

//获取异常内容
func (this *ExceptionBase) GetData() interface{} {
	return this.data
}

//获取异常文本
func (this *ExceptionBase) String() string {
	return fmt.Sprint(this.data)
}

//获取异常产生的堆栈
func (this *ExceptionBase) Stack() string {
	return string(debug.Stack())
}

func (this *ExceptionBase) Println(a ...interface{}) {
	fmt.Println(a...)
}
