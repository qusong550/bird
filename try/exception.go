//异常处理
package try

import (
	"fmt"
	"log"
	"runtime"
	"runtime/debug"
)

//默认异常处理方法
var defaultExceptionHandlerFunc = func(e Exception) {
	log.Println("异常：", e.GetMsg(), "\n", e.GetData(), "\n", string(debug.Stack()))
}

//设置未能捕捉的异常的统一处理方法
func SetExceptionHandler(fn ExceptionHandlerFunc) {
	if fn == nil {
		panic("不能注册空类型异常处理方法")
	}
	defaultExceptionHandlerFunc = fn
}

//异常接口
type Exception interface {
	Init(msg string, data interface{})
	Name() string         //异常名称
	GetMsg() string       //异常消息
	GetData() interface{} //异常数据
	String() string       //异常数据字符串输出
	Stack() string        //堆栈
}

//异常处理方法
type ExceptionHandlerFunc func(Exception)

//执行一个可能出现异常的程序段
func Do(fn func(), f ...ExceptionHandlerFunc) {
	defer func() {
		if content := recover(); content != nil {
			var handlerFunc = safeExceptionHandlerFunc(f...)
			switch e := content.(type) {
			case Exception: //预定义异常
				handlerFunc(e)
			case runtime.Error: //运行时异常
				ecp := &ExceptionRuntime{}
				ecp.Init("系统异常", e)
				handlerFunc(ecp)
			default: //其它异常
				ecp := &ExceptionBase{}
				ecp.Init("未知异常", e)
				handlerFunc(ecp)
			}
		}
	}()
	fn()
}

//抛出一个异常，【请谨慎使用，有可能因此造成程序退出】
//参数：Exception 或者其它错误
//如果第一个参数是Exception,则args可以增加title和data
//例如：Throw(&ExceptionBase{}, "未知异常","异常内容")
func Throw(e interface{}, args ...interface{}) {
	switch data := e.(type) {
	case Exception:
		switch len(args) {
		case 1:
			data.Init(fmt.Sprint(args[0]), nil)
		case 2:
			data.Init(fmt.Sprint(args[0]), args[1])
		}
		panic(data)
	default:
		panic(data)
	}
}

//开启一个可以捕捉异常的安全协程，建议程序中全部使用此方法启动协程
//必须对协程做异常处理,catch里面不能再次Throw
func Go(fn func(), f ...ExceptionHandlerFunc) {
	go Do(fn, f...)
}

//安全的异常包裹，避免异常内再抛出异常
func safeExceptionHandlerFunc(fn ...ExceptionHandlerFunc) ExceptionHandlerFunc {
	return func(e Exception) {
		/*
			defer func() {
				if err := recover(); err != nil && defaultExceptionHandlerFunc != nil {
					e = &ExceptionBase{}
					e.Init("异常中抛出了异常", err)
					defaultExceptionHandlerFunc(e)
				}
			}()
		*/
		if len(fn) > 0 {
			fn[0](e)
		}
	}
}
