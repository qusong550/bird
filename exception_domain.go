package bird

import (
	"log"
	"sync"

	"gitee.com/web-bird/bird/assets/utils"

	pconfig "gitee.com/web-bird/bird/assets/config"
	"gitee.com/web-bird/bird/try"
)

//异常类型
type ExceptionInfo struct {
	Id         string `json:"id"`         //异常id
	Name       string `json:"name"`       //异常名称
	Code       int    `json:"code"`       //返回的错误代码
	LogLevel   int    `json:"logLevel"`   //日志记录级别
	ModuleName string `json:"moduleName"` //模块名称
}

//
type exceptionDomain struct {
	exceptions map[string]*ExceptionInfo
	sync.Mutex
}

//
var _exceptionDomain *exceptionDomain

//
func ExceptionDomain() *exceptionDomain {
	if _exceptionDomain == nil {
		object := new(exceptionDomain)
		object.exceptions = make(map[string]*ExceptionInfo)
		_exceptionDomain = object
	}
	return _exceptionDomain
}

//注册配置
func (this *exceptionDomain) load(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, except := range mod.Exceptions() {
		var structName = utils.StructName(except)
		if _, ok := this.exceptions[structName]; ok {
			try.ThrowFatalf("不允许注册相同结构体名称的异常", structName)
		}
		this.exceptions[structName] = &ExceptionInfo{
			Id:         structName,
			Name:       except.Name(),
			ModuleName: mod.ModuleName(),
		}
	}
}

func (this *exceptionDomain) unload(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, except := range mod.Exceptions() {
		var structName = utils.StructName(except)
		delete(this.exceptions, structName)
	}
}

//初始化异常组件
func (this *exceptionDomain) init() {
	exceptionInfoMap := make(map[string]*ExceptionInfo)
	_ = pconfig.Read(this.filename(), &exceptionInfoMap)
	for _, e := range this.exceptions {
		if d, ok := exceptionInfoMap[e.Id]; ok {
			e.Code = d.Code
			e.LogLevel = d.LogLevel
		}
	}
	this.save()
}

//异常配置文件名
func (this *exceptionDomain) filename() string {
	return "./config/exception.data.json"
}

//保存数据
func (this *exceptionDomain) save() {
	if err := pconfig.Write(this.filename(), this.exceptions); err != nil {
		try.ThrowFatal("异常配置文件写入失败", err.Error())
	}
}

//设置
func (this *exceptionDomain) Set(id string, code int, logLevel int) {
	this.Lock()
	defer this.Unlock()
	e, ok := this.exceptions[id]
	if !ok {
		try.ThrowWarn("未注册的异常类型")
	}
	e.Code = code
	e.LogLevel = logLevel
	this.save()
}

//异常信息列表
func (this *exceptionDomain) Select(moduleName string) []*ExceptionInfo {
	list := make([]*ExceptionInfo, 0)
	for _, e := range this.exceptions {
		if moduleName != "" && e.ModuleName != moduleName {
			continue
		}
		list = append(list, &ExceptionInfo{
			Id:         e.Id,
			Name:       e.Name,
			Code:       e.Code,
			LogLevel:   e.LogLevel,
			ModuleName: e.ModuleName,
		})
	}
	return list
}

//异常处理器，如果捕捉的异常是一个未注册的异常，则使用ExceptionBase处理
//返回异常错误码
func (this *exceptionDomain) handler(b *Bird, e try.Exception) int {
	var code int
	try.Do(func() {
		structName := utils.StructName(e)
		var twice bool
	again:
		this.Lock()
		exceptionInfo, ok := this.exceptions[structName]
		this.Unlock()
		if !ok {
			if twice {
				return
			}
			structName = utils.StructName(try.ExceptionBase{})
			twice = true
			goto again
		}
		b.logSave(exceptionInfo.LogLevel, e.GetMsg(), e)
		code = exceptionInfo.Code
	}, func(e Exception) {
		log.Println("异常中出现异常：", e.GetMsg(), e.GetData(), e.Stack())
	})
	return code
}

//
func (this *exceptionDomain) AutoSave(b *Bird, e try.Exception) {
	this.handler(b, e)
}
