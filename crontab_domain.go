package bird

import (
	"sync"

	"gitee.com/web-bird/bird/assets/crontab"
	"gitee.com/web-bird/bird/try"
)

type crontabDomain struct {
	pool map[string]bool
	sync.Mutex
}

var _crontabDomain *crontabDomain

// 定时任务功能域
func CrontabDomain() *crontabDomain {
	if _crontabDomain == nil {
		object := new(crontabDomain)
		object.pool = make(map[string]bool)
		_crontabDomain = object
	}
	return _crontabDomain
}

// 加载
func (this *crontabDomain) load(mod ModuleInterface) {
	for _, cron := range mod.Crontabs() {
		key := mod.ModuleName() + cron.Name()
		if this.pool[key] {
			continue
		}
		this.pool[key] = true
		if err := this.run(mod.ModuleName(), cron); err != nil {
			try.ThrowFatalf("定时任务时间错误", "%s，错误:%s", cron.Name(), err.Error())
		}
	}
}

func (this *crontabDomain) run(modName string, cron CrontabInterface) error {
	var runLock sync.Mutex
	return crontab.NewCron(cron.Expression(), func() {
		if !ModuleDomain().IsLoaded(modName) {
			return
		}
		bird := NewBird().setClass(BIRD_CLASS_CRONTAB).setName(cron.Name())
		try.Do(func() {
			lockOk := runLock.TryLock()
			if !lockOk {
				return
			}
			defer runLock.Unlock()
			cron.Handler(bird)
		}, func(e try.Exception) {
			ExceptionDomain().handler(bird, e)
		})
	})
}

func (this *crontabDomain) unload(mod ModuleInterface) {

}

// 查询结构
type CrontabInfo struct {
	ModuleName string `json:"moduleName"`
	Name       string `json:"name"`       //名称
	Expression string `json:"expression"` //表达式
	crontab    CrontabInterface
}

// 可根据模块查询
func (this *crontabDomain) Select(moduleName string) []*CrontabInfo {
	list := make([]*CrontabInfo, 0)
	for _, mod := range ModuleDomain().modules {
		if moduleName != "" && moduleName != mod.ModuleName() {
			continue
		}
		for _, cron := range mod.Crontabs() {
			list = append(list, &CrontabInfo{
				Name:       cron.Name(),
				Expression: cron.Expression(),
				ModuleName: mod.ModuleName(),
			})
		}
	}
	return list
}
