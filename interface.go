package bird

import (
	"gitee.com/web-bird/bird/try"

	"github.com/gin-gonic/gin"
)

/*
	定义各种接口
*/

//应用接口
type AppInterface interface {
	Init(*gin.Engine)                                 //站点初始化执行方法(可以放一些中间件处理的东西)
	Modules() []ModuleInterface                       //加载需要的模块
	PermissionHandler(*gin.Context, *Permission) bool //权限处理(使用Throw中断）
	Install(*Bird)                                    //安装入口
	Debug() func()                                    //调试方法，如果此方法不为空，则不启动web服务且优先执行，专门用于紧急调试使用
}

//从app目录启动
type fromBirder interface {
	FromBird()
}

//模块接口
type ModuleInterface interface {
	Install(*Bird)
	ModuleName() string               //模块名称
	Init(*gin.Engine)                 //模块初始化，此处可放置路由
	Exceptions() []Exception          //异常列表
	Crontabs() []CrontabInterface     //定时任务
	Queues() []QueueInterface         //队列
	Components() []ComponentInterface //组件
	Hooks() []*Hook                   //钩子列表（注册后的事件才能使用）
	Events() []EventInterface         //事件列表（自动寻找对应钩子）
	Dicts() []*Dict                   //数据字典
}

//组件接口
type ComponentInterface interface {
	Models() []ModelInterface //数据库模型
}

//配置组件接口（用于判断组件是否包含配置）
type Configer interface {
	Config() ConfigInterface
}

//模型接口
type ModelInterface interface {
	TableName() string
}

//配置接口
type ConfigInterface interface {
	Default() ConfigInterface //默认配置
	Name() string             //配置名称
	Alias() string            //配置别名
	Validate() error          //验证
}

//定时任务接口
type CrontabInterface interface {
	Name() string       //任务名称
	Expression() string //定时时间表达式
	Handler(*Bird)      //执行方法
}

//队列任务接口
type QueueInterface interface {
	Name() string         //任务名称【唯一】
	Alias() string        //任务别名【唯一】
	Handler(*Bird, *Task) //执行方法
}

//事件接口
type EventInterface interface {
	HookId() string                       //事件id
	Handler(b *Bird, args ...interface{}) //事件处理方法
}

//日志处理器接口
type Logger interface {
	Save(log *Log)
}

//队列获取器接口
type TaskStoreInterface interface {
	TaskGet() *Task        //获取一个话题的任务
	TaskDone(*Bird, *Task) //完成一个任务
	TaskErr(*Bird, *Task)  //任务失败
}

//异常接口
type Exception = try.Exception
