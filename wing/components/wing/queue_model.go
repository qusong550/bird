package wing

import (
	"time"

	"gitee.com/web-bird/bird/assets/orm"
)

//队列数据库模型
type QueueModel struct {
	Id         int64
	QueueAlias string    `xorm:"varchar(64)"` //队列别名
	Arg        string    `xorm:"text"`        //参数
	Err        string    `xorm:"text"`        //错误数据
	CreatedAt  time.Time `xorm:"created datetime"`
	UpdatedAt  time.Time `xorm:"updated datetime"`
	Version    int       `xorm:"version"`
}

//
type queueModel struct {
	QueueModel `xorm:"extends"`
	orm.Model  `xorm:"-"`
}

//创建模型
func (this *Component) NewQueue() *queueModel {
	return this.Model(new(queueModel)).(*queueModel)
}

//
func (this *queueModel) TableName() string {
	return "Bird_Queue"
}

//
func (this *queueModel) PrimaryKey() interface{} {
	return this.Id
}
