package wing

import (
	"time"

	"gitee.com/web-bird/bird/assets/orm"
)

//日志结构
type LogModel struct {
	Id         int64
	BirdId     int64     `xorm:"bigint(20)"`         //保证是一个请求发起的
	ModuleName string    `xorm:"varchar(32) index"`  //模块名称
	BirdClass  int       `xorm:"tinyint(4) index"`   //鸟类
	BirdName   string    `xorm:"varchar(255) index"` //鸟名
	Msg        string    `xorm:"varchar(255) index"` //日志消息
	Level      int       `xorm:"tinyint(4) index"`   //日志等级
	Handler    string    `xorm:"varchar(32) index"`  //操作人
	Stack      string    `xorm:"text"`               //日志堆栈
	IP         string    `xorm:"varchar(32) index"`  //操作ip
	Data       string    `xorm:"text"`               //日志数据
	CreatedAt  time.Time `xorm:"created datetime"`
}

type logModel struct {
	LogModel  `xorm:"extends"`
	orm.Model `xorm:"-"`
}

//
func (this *Component) NewLog() *logModel {
	return this.Model(new(logModel)).(*logModel)
}

//
func (this *logModel) PrimaryKey() interface{} {
	return this.Id
}

//
func (this *logModel) TableName() string {
	return "Bird_Log"
}
