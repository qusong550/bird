package wing

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitee.com/web-bird/bird/try"

	"gitee.com/web-bird/bird"
)

type queueDomain struct {
	*Component
}

//初始化域
func (this *Component) QueueDomain() *queueDomain {
	return &queueDomain{Component: this}
}

//获取一个队列任务,如果获取不到则等待5秒再获取
//注：该方法为框架注入使用，其它地方不要使用
func (this *queueDomain) TaskGet() *bird.Task {
	queue := this.NewQueue()
	ok, err := this.DB().Asc("Id").Limit(1).Get(queue)
	this.IfErrorFatal(err)
	if !ok {
		return nil
	}
	task := new(bird.Task)
	task.QueueAlias = queue.QueueAlias
	task.Arg = queue.Arg
	task.QueueId = queue.Id
	return task
}

//任务完成自动删除队列
//注：该方法为框架注入使用，其它地方不要使用
func (this *queueDomain) TaskDone(b *bird.Bird, task *bird.Task) {
	queue := this.NewQueue()
	queue.ExtendEngine(b)
	if !queue.Where("Id=?", task.QueueId).Delete() {
		try.ThrowInfo("队列清除失败")
	}
}

func (this *queueDomain) TaskErr(b *bird.Bird, task *bird.Task) {
	queue := this.NewQueue()
	queue.ExtendEngine(b)
	queueInfo := this.NewQueue()
	if !queueInfo.Where("`Id`=?", task.QueueId).Get() {
		return
	}
	queueInfo.Err = task.Err
	if !queueInfo.Cols("Err").Update() {
		try.ThrowInfo("队列错误写入日志失败")
	}
}

//发布一个队列任务,可以同时关注多个topic
func (this *queueDomain) Publish(data interface{}, queueAlias ...string) {
	argByte, err := json.Marshal(data)
	if err != nil {
		try.ThrowFatalf("队列参数错误", "队列名：%s，错误：%s", strings.Join(queueAlias, ","), err.Error())
	}
	argStr := string(argByte)
	queues := make([]*queueModel, 0)
	birdQueueDomain := bird.QueueDomain()
	for _, alias := range queueAlias {
		if birdQueueDomain.GetName(alias) == "" {
			try.ThrowWarn(fmt.Sprintf("无效的队列方法%s", alias))
		}
		queue := this.NewQueue()
		queue.QueueAlias = alias
		queue.Arg = argStr
		queues = append(queues, queue)
	}
	this.Transaction(func() {
		_, err = this.Table(this.NewQueue()).Insert(&queues)
		this.IfErrorFatal(err)
	})
}

//查询进行中的队列
func (this *queueDomain) Find(queueAlias string, page, limit int, listPtr interface{}) int64 {
	db := this.Table(this.NewQueue())
	if queueAlias != "" {
		db.Where("QueueAlias=?", queueAlias)
	}
	db.Desc("Id").Cols("Id", "QueueAlias", "Arg", "CreatedAt", "UpdatedAt")
	return this.FindPage(db, listPtr, page, limit)
}
