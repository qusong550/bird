package wing

import (
	"sync"

	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/assets/utils"
	"gitee.com/web-bird/bird/try"
)

type logDomain struct {
	*Component
}

//
func (this *Component) LogDomain() *logDomain {
	return &logDomain{this}
}

var saveLock = new(sync.Map)

//保存日志，如果日志等级小于配置的则不保存,异常数据将保存其堆栈信息
func (this *logDomain) Save(l *bird.Log) {
	config := this.Config().(*Config)
	if l.Level < config.LogLevel {
		return
	}
	log := this.NewLog()
	log.BirdId = l.BirdId
	log.BirdClass = l.BirdClass
	log.BirdName = l.BirdName
	log.Level = l.Level
	log.Handler = l.Handler
	log.IP = l.IP
	log.Msg = l.Msg
	switch d := l.Data.(type) {
	case try.Exception:
		log.Stack = d.Stack()
		log.Data = d.String()
	case string:
		log.Data = d
	default:
		return
	}
	if stackRune := []rune(log.Stack); len(stackRune) > 10000 {
		log.Stack = string(stackRune[:10000])
	}
	if dataRune := []rune(log.Data); len(dataRune) > 10000 {
		log.Data = string(dataRune[:10000])
	}
	key := utils.Md5(log.BirdClass, log.Msg, log.BirdName, log.Level, log.Handler, log.IP, log.Data)
	if _, ok := saveLock.Load(key); ok {
		return
	}
	saveLock.Store(key, true)
	log.Insert()
}

//日志查询参数
type LogFindParams struct {
	Msg       string //
	BirdId    int64  //鸟
	BeginTime string //开始时间
	OverTime  string //结束时间
	BirdClass int    //鸟类型
	BirdName  string //鸟名
	Level     string //等级
	Handler   string //操作者
	IP        string //操作ip
}

//查询日志
func (this *logDomain) Find(args LogFindParams, page, limit int, listPtr interface{}) int64 {
	db := this.Table(this.NewLog())
	if args.BeginTime != "" {
		db.And("CreatedAt>?", args.BeginTime)
	}
	if args.OverTime != "" {
		db.And("CreatedAt<?", args.OverTime)
	}
	if args.Level != "" {
		db.And("Level=?", args.Level)
	}
	if args.BirdId > 0 {
		db.And("BirdId=?", args.BirdId)
	}
	if args.BirdClass > 0 {
		db.And("BirdClass=?", args.BirdClass)
	}
	if args.BirdName != "" {
		db.And("BirdName like ?", "%"+args.BirdName+"%")
	}
	if args.Handler != "" {
		db.And("Handler=?", args.Handler)
	}
	if args.IP != "" {
		db.And("IP=?", args.IP)
	}
	if args.Msg != "" {
		db.And("Msg=?", args.Msg)
	}
	db.Cols("Id", "CreatedAt", "BirdId", "BirdName", "Level", "Msg", "Handler", "IP", "BirdClass", "Data", "Stack")
	db.Desc("Id")
	return this.FindPage(db, listPtr, page, limit)
}

//根据id查询日志
func (this *logDomain) GetById(id int64) (*logModel, bool) {
	model := this.NewLog()
	ok := model.Where("Id=?", id).Get()
	return model, ok
}
