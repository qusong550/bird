package wing

import (
	"gitee.com/web-bird/bird"
)

type Component struct {
	*bird.Bird
}

func New(b *bird.Bird) *Component {
	object := new(Component)
	object.Bird = b
	return object
}

//
func (this *Component) Config() bird.ConfigInterface {
	return this.ConfigInit(&Config{})
}

//
func (this *Component) Models() []bird.ModelInterface {
	return []bird.ModelInterface{
		this.NewLog(),
		this.NewQueue(),
	}
}

//登录
func (this *Component) Login(username, password string) bool {
	config := this.Config().(*Config)
	if !config.UseDashboard {
		return false
	}
	return config.Username == username && config.Password == config.Password
}
