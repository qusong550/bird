package wing

import (
	"time"

	"gitee.com/web-bird/bird/assets/utils"

	"gitee.com/web-bird/bird"
)

type Config struct {
	CrossOpen     bool          `name:"是否允许跨域访问"`
	Route         string        `name:"管理路由" desc:"重启后生效"`
	UseDashboard  bool          `name:"是否启用管理面板" desc:"默认启用"`
	Username      string        `name:"管理账号" desc:"登录账号为字母数字组合" valid:"account:登录账号为字母数字组合"`
	Password      string        `name:"管理密码" mode:"password" desc:"密码必须包含大小写字母、数字三种数据类型" valid:"password2:密密码必须包含大小写字母、数字三种数据类型"`
	LogLevel      int           `name:"日志保存等级" mode:"single" options:"1:调试 2:记录 3:警告 4:致命" desc:"超过该等级才保存日志"`
	LogSaveDays   int           `name:"日志保存天数" desc:"超过后将自动清理" valid:"lte=30,gte=1:日志可保存时间为1-30天"`
	SignConfig    SignConfig    `name:"签名认证" desc:"采用Sha256加密，判断Sha256(token,secret)==sign，重启后生效"`
	VisitConfig   VisitConfig   `name:"访问限制" desc:"重启生效"`
	SessionConfig SessionConfig `name:"session配置" desc:"重启生效"`
	WhiteIP       []string      `name:"控制台访问IP白名单" desc:"重启生效"`
}

//签名中间件配置
type SignConfig struct {
	Open       bool   `name:"是否启用" desc:"默认不启用"`
	SignName   string `name:"签名Head头key值" desc:"默认Sign,长度为80字符"`
	SignSecret string `name:"-" desc:""`
}

//访问配置
type VisitConfig struct {
	MaxConnect      int   `name:"最大访问量" desc:"小于1则不限制，默认0"`
	RepeatCheckTime int64 `name:"重复提交检测时间" desc:"单位：秒，小于1则不限制，默认0"`
}

//
type SessionConfig struct {
	SessionName   string `name:"session名称" desc:"默认BIRD-ID"`
	MaxAge        int    `name:"session会话有效时长" desc:"单位：秒"`
	UseRedis      bool   `name:"是否启用reids" desc:"默认使用内存"`
	RedisHost     string `name:"redis服务器地址" desc:""`
	RedisPassword string `name:"redis服务器密码"`
	RedisDB       string `name:"redis数据库号"`
}

//默认配置
func (this *Config) Default() bird.ConfigInterface {
	this.CrossOpen = false
	this.Username = "Bird123"
	this.Password = utils.Sha1(time.Now())[:6]
	this.Route = "/bird"
	this.UseDashboard = false
	this.LogLevel = 3
	this.SignConfig = SignConfig{
		Open:       true,
		SignName:   "Sign",
		SignSecret: utils.Sha1(time.Now().Unix()),
	}
	this.SessionConfig = SessionConfig{
		SessionName: "BIRD-ID",
		MaxAge:      60 * 60 * 24 * 15,
	}
	this.LogSaveDays = 10
	return this
}

//配置名称
func (this *Config) Name() string {
	return "框架"
}

//配置别名
func (this *Config) Alias() string {
	return "freamwork"
}

func (this *Config) Validate() error {
	return nil
}
