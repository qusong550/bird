package controller

import (
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/try"
	"github.com/gin-gonic/gin"
	"strings"
)

type JWT struct {
	bird.JWT
	group string
}

//admin拦截
func JWTAdmin() *JWT {
	object := &JWT{group: "bird-wing"}
	object.Init(object)
	return object
}

//jwt用户分组
func (this *JWT) Group() string {
	return this.group
}

//获取token的方式
func (this *JWT) Token(c *gin.Context) string {
	var token string
	if c.Request.Header["Authorization"] != nil {
		token = c.Request.Header["Authorization"][0]
	}
	return strings.TrimSpace(token)
}

//当超时时,更新token
func (this *JWT) Timeout(c *gin.Context) {
	try.ThrowInfo("超时")
}
