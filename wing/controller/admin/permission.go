package admin

import (
	"gitee.com/web-bird/bird"
	"github.com/gin-gonic/gin"
)

type PermissionController struct {
}

//权限列表
func (PermissionController) List(c *gin.Context) {
	w := bird.NewContext(c)
	domain := bird.PermissionDomain()
	list := domain.Select()
	w.Res(100, list)
}
