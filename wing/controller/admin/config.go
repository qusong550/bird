package admin

import (
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/assets/binding"
	"gitee.com/web-bird/bird/try"
	"github.com/gin-gonic/gin"
)

type ConfigController struct {
}

//
func (ConfigController) List(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
	}
	w.BindQuery(&req)
	configDomain := bird.ConfigDomain()
	list := configDomain.Select(req.ModuleName)
	w.Res(100, list)
}

//
func (ConfigController) Info(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		Alias string `bind:"alias" valid:"required:配置别名不能为空"`
	}
	w.BindQuery(&req)
	configDomain := bird.ConfigDomain()
	structs, data := configDomain.GetInfo(req.Alias)
	w.Res(100, bird.H{
		"struct": structs, //结构
		"data":   data,    //数据
	})
}

//设置配置数据
func (ConfigController) Set(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		Alias  string                 `bind:"alias" valid:"required:配置别名不能为空"`
		Config map[string]interface{} `bind:"config" valid:"required:配置数据不能为空"`
	}
	w.BindJSON(&req)
	configDomain := bird.ConfigDomain()
	conf := configDomain.GetByAlias(req.Alias)
	if err := binding.Bind(conf, req.Config).Error(); err != nil {
		e := err.(*binding.BindError)
		try.Throw(&bird.ExceptionBind{Field: e.Field()}, e.Msg(), e.Error())
	}
	if err := conf.Validate(); err != nil {
		try.ThrowInfo(err.Error())
	}
	configDomain.SaveByPtr(conf)
	w.Code(100)
}
