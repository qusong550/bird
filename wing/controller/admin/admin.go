package admin

import (
	"fmt"
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/try"
	"gitee.com/web-bird/bird/wing/controller"
	"gitee.com/web-bird/bird/wing/service"
	"github.com/gin-gonic/gin"
)

type AdminController struct {
}

//登录
func (AdminController) Login(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		Username string `bind:"username" valid:"required:用户名不能为空 account:用户名错误"`
		Password string `bind:"password" valid:"required:密码不能为空 password2:密码错误"`
	}
	w.BindJSON(&req)
	wingService := service.NewWingServie(w.Bird)
	if !wingService.Login(req.Username, req.Password) {
		try.ThrowInfo("登录失败")
	}
	token, err := controller.JWTAdmin().GenerateToken(1, req.Username)
	if err != nil {
		info := fmt.Sprintf("登录用户：%s，账号：%s，错误：%s", req.Username, req.Password, err.Error())
		try.ThrowFatal("控制台登录失败", info)
	}
	w.Res(100, gin.H{
		"token": token,
	})
}

//登出
func (AdminController) Logout(c *gin.Context) {
	w := bird.NewContext(c)
	w.Code(100)
}
