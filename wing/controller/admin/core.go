package admin

import (
	"gitee.com/web-bird/bird"
	"github.com/gin-gonic/gin"
)

type CoreController struct {
}

//路由列表
func (CoreController) RouteList(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		Method string `bind:"method"`
		Path   string `bind:"path"`
	}
	w.BindQuery(&req)
	domain := bird.RouteDomain()
	list := domain.Select(req.Method, req.Path)
	w.Res(100, list)
}

//模块列表
func (CoreController) ModuleList(c *gin.Context) {
	w := bird.NewContext(c)
	domain := bird.ModuleDomain()
	list := domain.Select()
	w.Res(100, list)
}
