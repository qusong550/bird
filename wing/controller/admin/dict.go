package admin

import (
	"gitee.com/web-bird/bird"
	"github.com/gin-gonic/gin"
)

type DictController struct {
}

//字典列表
func (DictController) List(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
		Alias      string `bind:"alias"`
	}
	w.BindQuery(&req)
	domain := bird.DictDomain()
	list := domain.Select(req.ModuleName, req.Alias)
	w.Res(100, list)
}
