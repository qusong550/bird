package admin

import (
	"gitee.com/web-bird/bird"
	"github.com/gin-gonic/gin"
)

type ExceptionController struct {
}

//异常列表
func (ExceptionController) Set(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		Id       string `bind:"id" valid:"required:异常id不能为空"`
		Code     int    `bind:"code" valid:"required:状态码不能为空 not-in=0:状态码不要设为0"`
		LogLevel int    `bind:"logLevel"`
	}
	w.BindJSON(&req)
	domain := bird.ExceptionDomain()
	domain.Set(req.Id, req.Code, req.LogLevel)
	w.Code(100)
}

//异常列表
func (ExceptionController) List(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
	}
	w.BindQuery(&req)
	domain := bird.ExceptionDomain()
	list := domain.Select(req.ModuleName)
	w.Res(100, list)
}
