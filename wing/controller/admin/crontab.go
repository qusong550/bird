package admin

import (
	"gitee.com/web-bird/bird"
	"github.com/gin-gonic/gin"
)

type CrontabController struct {
}

//
func (CrontabController) List(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
	}
	w.BindQuery(&req)
	domain := bird.CrontabDomain()
	list := domain.Select(req.ModuleName)
	w.Res(100, list)
}
