package admin

import (
	"gitee.com/web-bird/bird"
	c_wing "gitee.com/web-bird/bird/wing/components/wing"
	"gitee.com/web-bird/bird/wing/service"
	"github.com/gin-gonic/gin"
)

type LogController struct {
}

//日志列表
func (LogController) List(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		BirdId    int64  `bind:"birdId"`    //
		BeginTime string `bind:"beginTime"` //开始时间
		Msg       string `bind:"msg"`       //消息标题
		OverTime  string `bind:"overTime"`  //结束时间
		BirdClass int    `bind:"birdClass"` //鸟类型
		BirdName  string `bind:"birdName"`  //鸟名
		Level     string `bind:"level"`     //等级
		Handler   string `bind:"handler"`   //操作者
		IP        string `bind:"ip"`        //操作ip
		bird.BaseQuery
	}
	w.BindQuery(&req)
	wingService := service.NewWingServie(w.Bird)
	list := make([]struct {
		Id        int64  `json:"id"`
		BirdId    string `json:"birdId"`
		Msg       string `json:"msg"`
		BirdClass int    `json:"birdClass"`
		BirdName  string `json:"birdName"`
		Level     int    `json:"level"`
		Handler   string `json:"handler"`
		IP        string `json:"ip"`
		Data      string `json:"data"`
		Stack     string `json:"stack"` //日志堆栈
		CreatedAt string `json:"createdAt"`
	}, 0)
	arg := c_wing.LogFindParams{
		BirdId:    req.BirdId,
		Msg:       req.Msg,
		BeginTime: req.BeginTime,
		OverTime:  req.OverTime,
		BirdClass: req.BirdClass,
		BirdName:  req.BirdName,
		Level:     req.Level,
		Handler:   req.Handler,
		IP:        req.IP,
	}
	total := wingService.LogDomain().Find(arg, req.Page, req.Limit, &list)
	w.Res(100, gin.H{
		"total": total,
		"data":  list,
	})
}
