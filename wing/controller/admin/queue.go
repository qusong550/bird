package admin

import (
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/wing/service"
	"github.com/gin-gonic/gin"
)

type QueueController struct {
}

//队列方法列表
func (QueueController) List(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
	}
	w.BindQuery(&req)
	domain := bird.QueueDomain()
	list := domain.Select(req.ModuleName)
	w.Res(100, list)
}

//任务列表
func (QueueController) TaskList(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		Alias string `bind:"alias"`
		bird.BaseQuery
	}
	w.BindQuery(&req)
	wingService := service.NewWingServie(w.Bird)
	list := make([]*struct {
		Id         int64  `json:"id"`
		QueueAlias string `json:"queueAlias"`
		QueueName  string `json:"name"`
		Arg        string `json:"arg"`
		CreatedAt  string `json:"createdAt"`
		UpdatedAt  string `json:"updatedAt"`
	}, 0)
	total := wingService.QueueDomain().Find(req.Alias, req.Page, req.Limit, &list)
	domain := bird.QueueDomain()
	for _, item := range list {
		item.QueueName = domain.GetName(item.QueueAlias)
	}
	w.Res(100, gin.H{
		"total": total,
		"data":  list,
	})
}
