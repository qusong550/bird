package admin

import (
	"gitee.com/web-bird/bird"
	"github.com/gin-gonic/gin"
)

type HookController struct {
}

//钩子列表
func (HookController) HookList(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
		HookId     string `bind:"hookId"`
	}
	w.BindQuery(&req)
	domain := bird.HookDomain()
	list := domain.SelectHook(req.ModuleName, req.HookId)
	w.Res(100, list)
}

//事件列表
func (HookController) EventList(c *gin.Context) {
	w := bird.NewContext(c)
	var req struct {
		ModuleName string `bind:"moduleName"`
		HookId     string `bind:"hookId"`
	}
	w.BindQuery(&req)
	domain := bird.HookDomain()
	list := domain.SelectEvent(req.ModuleName, req.HookId)
	w.Res(100, list)
}
