package crontabs

import (
	"time"

	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/wing/components/wing"
	"gitee.com/web-bird/bird/wing/service"
)

//
type AutoCleanLogs struct {
}

//任务名称
func (this *AutoCleanLogs) Name() string {
	return "自动清理日志"
}

//1小时执行一次
func (this *AutoCleanLogs) Expression() string {
	return "* * * * 0 0"
}

//清理过期充值和未付款订单，订单清理时先确保是未充值的
func (this *AutoCleanLogs) Handler(b *bird.Bird) {
	wingService := service.NewWingServie(b)
	config := wingService.Config().(*wing.Config)
	beforeDate := time.Now().AddDate(0, 0, 0-config.LogSaveDays).Format(bird.FORMAT_DATE)
	wingService.Table(wingService.NewLog()).Where("`CreatedAt`<?", beforeDate).Delete()
}
