package service

import (
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/wing/components/wing"
)

type QueueService struct {
	component *wing.Component
}

func NewQueueService(b *bird.Bird) *QueueService {
	object := new(QueueService)
	object.component = wing.New(b)
	return object
}

func (this *QueueService) Publish(data interface{}, topics ...string) {
	this.component.QueueDomain().Publish(data, topics...)
}

//创建队列store
func NewTaskStore(b *bird.Bird) bird.TaskStoreInterface {
	object := NewQueueService(b)
	return object.component.QueueDomain()
}
