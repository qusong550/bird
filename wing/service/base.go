package service

import (
	"gitee.com/web-bird/bird"
	c_wing "gitee.com/web-bird/bird/wing/components/wing"
)

type WingServie struct {
	*c_wing.Component
}

//
func NewWingServie(b *bird.Bird) *WingServie {
	object := new(WingServie)
	object.Component = c_wing.New(b)
	return object
}
