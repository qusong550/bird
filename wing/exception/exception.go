package exception

import (
	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/assets/orm"
	"gitee.com/web-bird/bird/try"
)

//基础异常
type ExceptionBase = try.ExceptionBase

//运行时异常
type ExceptionRuntime = try.ExceptionRuntime

//信息异常
type ExceptionInfo = try.ExceptionInfo

//警告异常
type ExceptionWarn = try.ExceptionWarn

//致命异常
type ExceptionFatal = try.ExceptionFatal

//数据库异常
type ExceptionSQL = orm.ExceptionSQL

//数据绑定异常
type ExceptionBind = bird.ExceptionBind

//权限异常
type ExceptionPermission = bird.ExceptionPermission

//登录异常
type ExceptionJWT = bird.ExceptionJWT
