package wing

import (
	"net/http"

	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/middleware"
	"gitee.com/web-bird/bird/try"
	c_wing "gitee.com/web-bird/bird/wing/components/wing"
	"gitee.com/web-bird/bird/wing/controller"
	"gitee.com/web-bird/bird/wing/controller/admin"
	"gitee.com/web-bird/bird/wing/controller/crontabs"
	"gitee.com/web-bird/bird/wing/exception"
	middleware2 "gitee.com/web-bird/bird/wing/middleware"
	"gitee.com/web-bird/bird/wing/service"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
)

type module struct {
}

//初始化模块
func New() bird.ModuleInterface {
	object := new(module)
	return object
}

//
func (this *module) ModuleName() string {
	return "Bird框架"
}

//模型初始化，此处可放置路由
func (this *module) Init(eg *gin.Engine) {
	//设置日志处理器
	logService := service.NewWingServie(bird.NewBird())
	bird.SetLogHandler(logService.LogDomain())
	//设置队列处理器
	bird.SetQueueTaskStore(service.NewTaskStore)
	//
	wingService := service.NewWingServie(bird.NewBird())
	//
	config := wingService.Config().(*c_wing.Config)

	//跨域访问
	if config.CrossOpen {
		eg.Use(func(c *gin.Context) {
			method := c.Request.Method
			c.Header("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
			c.Header("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE,OPTIONS")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
			//放行所有OPTIONS方法
			if method == "OPTIONS" {
				c.AbortWithStatus(http.StatusNoContent)
			}
			// 处理请求
			c.Next()
		})
	}
	dashboard := eg.Group(config.Route)
	//签名验证中间件
	eg.Use(middleware2.StartSign())
	//最大访问数量限制中间件，
	if config.VisitConfig.MaxConnect > 0 {
		eg.Use(middleware.MaxAllowed(config.VisitConfig.MaxConnect))
	}
	//重复提交过滤中间件
	if config.VisitConfig.RepeatCheckTime > 0 {
		eg.Use(middleware.Sequence(config.VisitConfig.RepeatCheckTime, func(c *gin.Context) {
			try.ThrowInfo("您的操作过于频繁，请稍后再试")
		}))
	}
	//session中间件
	var sessionStore sessions.Store
	var secret = []byte("bird-secret-" + config.SessionConfig.SessionName)
	if config.SessionConfig.UseRedis {
		var err error
		sessionStore, err = redis.NewStoreWithDB(10, "tcp", config.SessionConfig.RedisHost, config.SessionConfig.RedisPassword, config.SessionConfig.RedisDB, secret)
		if err != nil {
			try.ThrowFatal("redis连接失败", err.Error())
		}
	} else {
		sessionStore = memstore.NewStore(secret)
	}
	sessionStore.Options(sessions.Options{MaxAge: config.SessionConfig.MaxAge, Path: "/"})
	eg.Use(sessions.Sessions(config.SessionConfig.SessionName, sessionStore))

	//加载配置工具接口
	if config.Route == "" {
		return
	}
	dashboard.Use(func(c *gin.Context) {
		config := wingService.Config().(*c_wing.Config)
		if !config.UseDashboard {
			c.AbortWithStatus(404) //管理面板未启用
			return
		}
		if len(config.WhiteIP) > 0 {
			for _, ip := range config.WhiteIP {
				if c.ClientIP() == ip {
					return
				}
			}
			c.AbortWithStatus(404) //ip不匹配白名单
			return
		}
	})
	dashboard.POST("/admin/login", admin.AdminController{}.Login) //登录
	dashboard.Use(func(c *gin.Context) {                          //登录状态验证
		//拦截处理jwt登录
		try.Do(func() {
			controller.JWTAdmin().Handler()(c)
		}, func(e try.Exception) {
			switch e.(type) {
			case *exception.ExceptionJWT:
				w := bird.NewContext(c)
				w.Code(401)
			default:
				try.Throw(e)
			}
		})
	})
	dashboard.POST("/admin/logout", admin.AdminController{}.Logout) //退出登录
	this.RegRoute(dashboard)
}

//注册路由,可在其他模块调用此方法
func (this *module) RegRoute(group *gin.RouterGroup) {
	//配置
	config := group.Group("/config")
	{
		config.GET("/list", admin.ConfigController{}.List) //配置列表
		config.GET("/info", admin.ConfigController{}.Info) //配置详情
		config.PUT("/set", admin.ConfigController{}.Set)   //配置修改
	}
	//定时任务
	crontab := group.Group("/crontab")
	{
		crontab.GET("/list", admin.CrontabController{}.List) //定时任务列表
	}
	//队列
	queue := group.Group("/queue")
	{
		queue.GET("/list", admin.QueueController{}.List)      //队列列表
		queue.GET("/tasks", admin.QueueController{}.TaskList) //任务列表
	}
	//字典
	dict := group.Group("/dict")
	{
		dict.GET("/list", admin.DictController{}.List) //字典列表
	}
	//钩子
	hook := group.Group("/hook")
	{
		hook.GET("/list", admin.HookController{}.HookList)        //钩子列表
		hook.GET("/event/list", admin.HookController{}.EventList) //钩子事件列表
	}
	//日志
	log := group.Group("/log")
	{
		log.GET("/list", admin.LogController{}.List) //日志列表
	}
	//路由
	core := group.Group("/core")
	{
		core.GET("/route/list", admin.CoreController{}.RouteList)   //路由列表
		core.GET("/module/list", admin.CoreController{}.ModuleList) //模块列表
	}
	//异常
	except := group.Group("/exception")
	{
		except.GET("/list", admin.ExceptionController{}.List) //
		except.PUT("/set", admin.ExceptionController{}.Set)   //
	}
	//
	permission := group.Group("/permission")
	{
		permission.GET("/list", admin.PermissionController{}.List) //权限列表
	}
}

//注册异常类
func (this *module) Exceptions() []bird.Exception {
	return []bird.Exception{
		&exception.ExceptionBase{},
		&exception.ExceptionRuntime{},
		&exception.ExceptionInfo{},
		&exception.ExceptionWarn{},
		&exception.ExceptionFatal{},
		&exception.ExceptionSQL{},
		&exception.ExceptionBind{},
		&exception.ExceptionPermission{},
		&exception.ExceptionJWT{},
	}
}

//
func (this *module) Install(b *bird.Bird) {

}

//定时任务
func (this *module) Crontabs() []bird.CrontabInterface {
	return []bird.CrontabInterface{
		&crontabs.AutoCleanLogs{},
	}
}

//队列
func (this *module) Queues() []bird.QueueInterface {
	return []bird.QueueInterface{}
}

//组件
func (this *module) Components() []bird.ComponentInterface {
	bd := bird.NewBird()
	return []bird.ComponentInterface{
		service.NewWingServie(bd),
	}
}

//钩子列表（注册后的事件才能使用）
func (this *module) Hooks() []*bird.Hook {
	return []*bird.Hook{}
}

//事件列表（自动寻找对应钩子）
func (this *module) Events() []bird.EventInterface {
	return []bird.EventInterface{}
}

//数据字典
func (this *module) Dicts() []*bird.Dict {
	return []*bird.Dict{}
}
