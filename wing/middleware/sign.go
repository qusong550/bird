package middleware

import (
	"fmt"
	"strings"
	"sync"

	"gitee.com/web-bird/bird"
	"gitee.com/web-bird/bird/assets/utils"
	c_wing "gitee.com/web-bird/bird/wing/components/wing"
	"gitee.com/web-bird/bird/wing/service"
	"github.com/gin-gonic/gin"
)

//
func StartSign() gin.HandlerFunc {
	return func(c *gin.Context) {
		w := bird.NewContext(c)
		wingService := service.NewWingServie(w.Bird)
		conf := wingService.Config().(*c_wing.Config).SignConfig
		if sec, _ := c.Cookie("sec"); sec == conf.SignSecret {
			return
		}
		c.SetCookie("sec", conf.SignSecret, 36000, "/", "", false, false)
	}
}

var ignoreSignGroupsMap = make(map[string]bool)

//注册需要忽略的路由分组
func SignIngoreGroup(group string) {
	ignoreSignGroupsMap[group] = true
}

//签名中间件
func Sign() gin.HandlerFunc {
	//签名验证中间件
	//签名长度在74-104字符之间
	//signKey=sign的header key名
	//secret=密匙
	var signsMap = new(sync.Map)
	var signLength = 0
	return func(c *gin.Context) {
		w := bird.NewContext(c)
		wingService := service.NewWingServie(w.Bird)
		conf := wingService.Config().(*c_wing.Config).SignConfig
		if !conf.Open {
			return
		}
		//请求的路由组过滤，谨慎使用
		for group, _ := range ignoreSignGroupsMap {
			if strings.Index(c.Request.URL.Path, group) > -1 {
				return
			}
		}
		signLength++
		if signLength > 100000 {
			signsMap = new(sync.Map)
			signLength = 0
		}
		signStr := c.GetHeader(conf.SignName)
		if l := len(signStr); l < 74 || l > 104 {
			c.AbortWithStatus(403)
			return
		}
		signIndex := utils.Sha256(c.ClientIP(), c.Request.RequestURI, signStr)
		if _, ok := signsMap.Load(signIndex); ok {
			c.AbortWithStatus(403)
			return
		}
		start := len(signStr) - 64
		token := signStr[:start]
		sign := signStr[start:]
		if utils.Sha256(fmt.Sprint(token, conf.SignSecret)) != sign { //签名未通过
			c.AbortWithStatus(403)
			return
		}
		signsMap.Store(signIndex, true)
	}
}
