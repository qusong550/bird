package bird

import "gitee.com/web-bird/bird/assets/utils"

//加盐sha1
func Sha1(args ...interface{}) string {
	return utils.Sha1(append(args, config.MD5))
}

//加盐md5
func Md5(args ...interface{}) string {
	return utils.Md5(append(args, config.MD5))
}
