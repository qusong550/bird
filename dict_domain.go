package bird

import (
	"fmt"
	"gitee.com/web-bird/bird/try"
	"sync"
)

type dictDomain struct {
	dicts map[string]*Dict
	sync.Mutex
}

//
var _dictDomain *dictDomain

//
func DictDomain() *dictDomain {
	if _dictDomain == nil {
		object := new(dictDomain)
		object.dicts = make(map[string]*Dict)
		_dictDomain = object
	}
	return _dictDomain
}

//注册配置
func (this *dictDomain) load(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, dict := range mod.Dicts() {
		var alias = dict.Alias
		if _, ok := this.dicts[alias]; ok {
			try.ThrowFatal(fmt.Sprintf("字典【%s】重复", dict.Name))
		}
		dict.checkItems() //检查是否有重复的字典元素
		dict.ModuleName = mod.ModuleName()
		this.dicts[alias] = dict
	}
}

func (this *dictDomain) unload(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	for _, dict := range mod.Dicts() {
		delete(this.dicts, dict.Alias)
	}
}

//查询配置,可根据模块查询
func (this *dictDomain) Select(moduleName string, alias string) []*Dict {
	list := make([]*Dict, 0)
	for _, dict := range this.dicts {
		if moduleName != "" && moduleName != dict.ModuleName {
			continue
		}
		if alias != "" && dict.Alias != alias {
			continue
		}
		list = append(list, &Dict{
			Alias:      dict.Alias,
			Name:       dict.Name,
			Items:      dict.Items,
			ModuleName: dict.ModuleName,
		})
	}
	return list
}
