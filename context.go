package bird

import (
	"time"

	"gitee.com/web-bird/bird/assets/binding"
	"gitee.com/web-bird/bird/middleware"
	"gitee.com/web-bird/bird/try"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

//升级版context
type Context struct {
	*Bird   //继承鸟类
	Context *gin.Context
}

//存鸟类的key
const GIN_KEY_BIRD = "gin_key_bird"

//包装一个升级版的context
//继承了小鸟、gin的context
func NewContext(c *gin.Context) *Context {
	obj := new(Context)
	obj.Context = c
	bird, ok := c.Get(GIN_KEY_BIRD)
	if !ok {
		b := NewBird().setClass(BIRD_CLASS_ROUTER).setName(c.Request.RequestURI)
		b.birdIP = c.ClientIP()
		bird = b
		c.Set(GIN_KEY_BIRD, bird)
	}
	obj.Bird = bird.(*Bird)
	obj.Bird.Context = obj
	return obj
}

//获取session对象
func (this *Context) Session() sessions.Session {
	return sessions.Default(this.Context)
}

//获取jwt的Claims对象
func (this *Context) JWTClaims() *middleware.Claims {
	claims, ok := this.Context.Get(middleware.GIN_KEY_JWT)
	if !ok {
		return nil
	}
	return claims.(*middleware.Claims)
}

//session单例处理器，可以处理阅读、点击之类的数据
//如果session里面未保存key，则执行fn
func (this *Context) UniqueHandler(key string, fn func()) {
	if this.Session().Get(key) != nil {
		return
	}
	fn()
	this.Session().Set(key, true)
	if err := this.Session().Save(); err != nil {
		this.LogFatal("UniqueHandler session保存失败", err.Error())
	}
}

//当key在有效期expireMinutes内被调用了times次则执行fn
func (this *Context) MultiHandler(key string, expireMinutes, times int, fn func()) {
	defer func() {
		if err := this.Session().Save(); err != nil {
			this.LogFatal("MultiHandler session保存失败", err.Error())
		}
	}()
	keyStart := key + "start"
	var d int = 0
	if number := this.Session().Get(key); number != nil {
		d, _ = number.(int)
	}
	var startAt = time.Now().Unix()
	if start := this.Session().Get(keyStart); start == nil {
		this.Session().Set(keyStart, time.Now().Unix())
	} else {
		startAt, _ = start.(int64)
	}
	if time.Now().Unix()-startAt > int64(expireMinutes)*60 {
		this.Session().Delete(keyStart)
		d = 1
	}
	if d >= times {
		fn()
		this.Session().Delete(key)
	} else {
		this.Session().Set(key, d+1)
	}
}

//json输出
func (this *Context) JSON(code int, res interface{}, msg string) {
	const rspKey = "response"
	if _, ok := this.Context.Get(rspKey); ok {
		return
	}
	type ret struct {
		Code   int         `json:"code"`
		Result interface{} `json:"result"`
		Msg    string      `json:"msg"`
	}
	this.Context.Set(rspKey, true)
	this.Context.JSON(200, &ret{code, res, msg})
	/*
		this.Context.Header("Content-type","application/json;charset=utf-8")
		data:=binding.Marshal(&ret{code, res, msg})
		this.Context.String(200,data)
	*/
}

//只返回状态码
func (this *Context) Code(code int) {
	this.JSON(code, nil, "")
}

//成功返回（只输出data）
func (this *Context) Res(code int, data interface{}) {
	this.JSON(code, data, "")
}

//错误返回（只输出msg)
func (this *Context) Msg(code int, msg string) {
	this.JSON(code, nil, msg)
}

//绑定query
func (this *Context) BindQuery(objectPtr interface{}) *Context {
	err := binding.HttpBindQuery(this.Context.Request, objectPtr)
	this.ifBindError(err)
	return this
}

//绑定json
func (this *Context) BindJSON(objectPtr interface{}) *Context {
	err := binding.HttpBindJSON(this.Context.Request, objectPtr)
	this.ifBindError(err)
	return this
}

//绑定form
func (this *Context) BindForm(objectPtr interface{}) *Context {
	err := binding.HttpBindForm(this.Context.Request, objectPtr)
	this.ifBindError(err)
	return this
}

//绑定head
func (this *Context) BindHead(objectPtr interface{}) *Context {
	err := binding.HttpBindForm(this.Context.Request, objectPtr)
	this.ifBindError(err)
	return this
}

//绑定cookie
func (this *Context) BindCookie(objectPtr interface{}) *Context {
	err := binding.HttpBindForm(this.Context.Request, objectPtr)
	this.ifBindError(err)
	return this
}

//如果数据绑定出错
func (this *Context) ifBindError(err error) {
	if err == nil {
		return
	}
	switch e := err.(type) {
	case *binding.BindError:
		try.Throw(&ExceptionBind{Field: e.Field()}, e.Msg(), e.Error())
	case error:
		try.ThrowFatal("请求参数错误", err.Error())
	}
}
