package bird

import (
	"fmt"
	"gitee.com/web-bird/bird/try"
	"log"
)

//日志等级

const (
	LOG_DEBUG = 1
	LOG_INFO  = 2
	LOG_WARN  = 3
	LOG_FATAL = 4
)

//日志结构
type Log struct {
	BirdId    int64       //保证是一个请求发起的
	Msg       string      //日志标题
	BirdClass int         //鸟类
	BirdName  string      //鸟名
	Level     int         //日志等级
	Handler   string      //操作人
	IP        string      //操作ip
	Data      interface{} //错误内容
}

//日志处理器
var logger Logger

//设置日志处理器
func SetLogHandler(l Logger) {
	logger = l
}

//给鸟类增加日志保存处理
//
func (this *Bird) LogDebug(msg string, arg ...interface{}) {
	this.logSave(LOG_DEBUG, msg, fmt.Sprint(arg...))
}

//
func (this *Bird) LogDebugf(msg string, fomrat string, arg ...interface{}) {
	this.logSave(LOG_DEBUG, msg, fmt.Sprintf(fomrat, arg...))
}

//
func (this *Bird) LogInfo(msg string, arg ...interface{}) {
	this.logSave(LOG_INFO, msg, fmt.Sprint(arg...))
}

//
func (this *Bird) LogInfof(msg string, fomrat string, arg ...interface{}) {
	this.logSave(LOG_INFO, msg, fmt.Sprintf(fomrat, arg...))
}

//
func (this *Bird) LogWarn(msg string, arg ...interface{}) {
	this.logSave(LOG_WARN, msg, fmt.Sprint(arg...))
}

//
func (this *Bird) LogWarnf(msg string, fomrat string, arg ...interface{}) {
	this.logSave(LOG_WARN, msg, fmt.Sprintf(fomrat, arg...))
}

//
func (this *Bird) LogFatal(msg string, arg ...interface{}) {
	this.logSave(LOG_FATAL, msg, fmt.Sprint(arg...))
}

//
func (this *Bird) LogFatalf(msg string, fomrat string, arg ...interface{}) {
	this.logSave(LOG_FATAL, msg, fmt.Sprintf(fomrat, arg...))
}

//保存日志
func (this *Bird) logSave(level int, msg string, data interface{}) {
	if logger == nil {
		return
	}
	//调用外部的方法，采用不信任模式
	try.Do(func() {
		logger.Save(&Log{
			BirdId:    this.birdId,
			BirdClass: this.birdClass,
			BirdName:  this.birdName,
			Level:     level,
			Handler:   this.handler,
			IP:        this.birdIP,
			Msg:       msg,
			Data:      data,
		})
	}, func(e Exception) {
		log.Println("日志处理异常：", e.Name(), e.GetMsg(), e.String())
		//此处应有钩子通知
	})
}
