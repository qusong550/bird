package bird

import (
	"fmt"
	"sync"

	"gitee.com/web-bird/bird/try"
)

type moduleDomain struct {
	pool    map[string]*ModuleInfo
	modules map[string]ModuleInterface
	inited  map[string]bool
	sync.Mutex
}

var _moduleDomain *moduleDomain

//模块处理域
func ModuleDomain() *moduleDomain {
	if _moduleDomain == nil {
		object := new(moduleDomain)
		object.modules = make(map[string]ModuleInterface)
		object.pool = make(map[string]*ModuleInfo)
		object.inited = make(map[string]bool)
		_moduleDomain = object
	}
	return _moduleDomain
}

//
func (this *moduleDomain) IsLoaded(modName string) bool {
	_, ok := this.modules[modName]
	return ok
}

type ModuleInfo struct {
	Name       string `json:"name"`
	Components int    `json:"components"` //组件数量
	Dicts      int    `json:"dicts"`      //字典数量
	Events     int    `json:"events"`     //注册事件数量
	Hooks      int    `json:"hooks"`      //钩子数量
	Tables     int    `json:"tables"`     //数据库数量
	Configs    int    `json:"configs"`    //配置列表
	module     ModuleInterface
}

func (this *moduleDomain) load(mod ModuleInterface) {
	if this.IsLoaded(mod.ModuleName()) {
		try.ThrowFatal(fmt.Sprintf("模块【%s】重复加载", mod.ModuleName()))
	}
	this.Lock()
	defer this.Unlock()
	this.modules[mod.ModuleName()] = mod
	info := &ModuleInfo{
		Name:       mod.ModuleName(),
		Components: len(mod.Components()),
		Dicts:      len(mod.Dicts()),
		Events:     len(mod.Events()),
		Hooks:      len(mod.Hooks()),
		module:     mod,
	}
	for _, c := range mod.Components() {
		if _, ok := c.(Configer); ok {
			info.Configs++
		}
		info.Tables += len(c.Models())
	}
	this.pool[mod.ModuleName()] = info
	//定时任务
	CrontabDomain().load(mod)
	//钩子
	HookDomain().load(mod)
	//队列
	QueueDomain().load(mod)
	//字典
	DictDomain().load(mod)
	//
	ConfigDomain().load(mod)
	//异常
	ExceptionDomain().load(mod)
}

//
func (this *moduleDomain) Load(mod ModuleInterface) {
	this.load(mod)
	if _, ok := this.inited[mod.ModuleName()]; !ok {
		mod.Init(CoreDomain().server)
		this.inited[mod.ModuleName()] = true
	}
}

//卸载
func (this *moduleDomain) Unload(mod ModuleInterface) {
	this.Lock()
	defer this.Unlock()
	delete(this.modules, mod.ModuleName())
	delete(this.pool, mod.ModuleName())
	//定时任务
	CrontabDomain().unload(mod)
	//事件
	HookDomain().unload(mod)
	//队列
	QueueDomain().unload(mod)
	//字典
	DictDomain().unload(mod)
	//
	ConfigDomain().unload(mod)
	//异常
	ExceptionDomain().unload(mod)
}

//注册列表,将控制器，队列，定时任务全部注册
func (this *moduleDomain) init(modules []ModuleInterface) {
	for _, mod := range modules {
		this.load(mod)
	}
	for _, mod := range modules {
		mod.Init(CoreDomain().server)
	}
}

//单独安装模块数据库
func (this *moduleDomain) Install(module ModuleInterface) {
	bd := NewBird()
	models := make([]interface{}, 0)
	for _, item := range module.Components() {
		for _, model := range item.Models() {
			models = append(models, model)
		}
	}
	if err := bd.DB().Sync2(models...); err != nil {
		try.ThrowFatal(module.ModuleName()+"安装失败", err.Error())
	}
	module.Install(bd)
}

//获取所有的模型
func (this *moduleDomain) GetModels() []ModelInterface {
	models := make([]ModelInterface, 0)
	for _, module := range this.modules {
		for _, item := range module.Components() {
			for _, model := range item.Models() {
				models = append(models, model)
			}
		}
	}
	return models
}

func (this *moduleDomain) install(modules []ModuleInterface) {
	for _, module := range modules {
		this.Install(module)
	}
}

//模块列表
func (this *moduleDomain) Select() []*ModuleInfo {
	list := make([]*ModuleInfo, 0)
	for _, mod := range this.pool {
		list = append(list, &ModuleInfo{
			Name:       mod.Name,
			Components: mod.Components,
			Dicts:      mod.Dicts,
			Events:     mod.Events,
			Hooks:      mod.Hooks,
		})
	}
	return list
}
