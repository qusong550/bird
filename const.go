package bird

//日期格式
const (
	FORMAT_DATE     = "2006-01-02"
	FORMAT_DATETIME = "2006-01-02 15:04:05"
)

const (
	CONFIG_DIR  = "./config"
	RUNTIME_DIR = "./runtime"
)
