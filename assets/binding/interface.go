package binding

//json序列化接口
type SerializeInterface interface {
	UnmarshalJSON(data []byte) error
	MarshalJSON() ([]byte, error)
}
