package config

import (
	"encoding/json"
	"testing"
)

type User struct {
	Name string
	Age  int
	Path struct {
		Host string
	}
}

func TestConfig(t *testing.T) {

	return
	u := &User{Name: "张三"}
	s := `{"Age":28}`
	json.Unmarshal([]byte(s), u)
	t.Log(u)
	return

	wuser := &User{Name: "张三", Age: 30}
	const filename = "config.json"
	if err := Write(filename, wuser); err != nil {
		t.Log("配置写入失败：", err.Error())
	}

	ruser := &User{}
	if err := Read(filename, ruser); err != nil {
		t.Log("配置读取失败：", err.Error())
	} else {
		t.Log("读取结果：", ruser)
	}
}
