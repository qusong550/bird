package utils

import "database/sql"

//测试数据库是否可以正常连接
func TestSqlConnect(driverName string, dataSourceName string) error {
	s, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return err
	}
	return s.Ping()
}
