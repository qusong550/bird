package utils

import "sync"

type MapInt64 struct {
	data map[string]int64
	sync.Mutex
}

func NewMapInt64() *MapInt64 {
	return &MapInt64{data: make(map[string]int64)}
}

func (this *MapInt64) Data() map[string]int64 {
	return this.data
}

func (this *MapInt64) Set(key string, val int64) {
	this.Lock()
	defer this.Unlock()
	this.data[key] = val
}

func (this *MapInt64) Get(key string) int64 {
	this.Lock()
	defer this.Unlock()
	return this.data[key]
}

func (this *MapInt64) Has(key string) (int64, bool) {
	this.Lock()
	defer this.Unlock()
	data, has := this.data[key]
	return data, has
}

func (this *MapInt64) Add(key string, val int64) {
	this.Lock()
	defer this.Unlock()
	this.data[key] += val
}
