package utils

import (
	"fmt"
	"os"
)

type Log struct {
	f *os.File
}

func NewLog(file string) *Log {
	object := new(Log)
	var err error
	object.f, err = os.OpenFile(file, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		panic("日志" + file + "创建失败:" + err.Error())
	}
	return object
}

func (this *Log) Save(args ...interface{}) {
	this.f.WriteString(fmt.Sprint(args...))
}

//
func (this *Log) Savef(format string, args ...interface{}) {
	this.f.WriteString(fmt.Sprintf(format, args...))
}
