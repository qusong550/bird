package utils

import (
	"fmt"
	"testing"
)

func TestArrayIn(t *testing.T) {
	fmt.Println(ArrayIn("1", "a", "b", "c"))
	fmt.Println(ArrayIn("a", "a", "b", "c"))
	fmt.Println(ArrayIn(1, 1, "b", "c"))
}
