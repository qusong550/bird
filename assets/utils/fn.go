package utils

import (
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

//获取当前时间的星期一时间
func GetMonday(t time.Time) time.Time {
	m := map[string]int{
		"Monday":    0,
		"Tuesday":   -1,
		"Wednesday": -2,
		"Thursday":  -3,
		"Friday":    -4,
		"Saturday":  -5,
		"Sunday":    -6,
	}
	day := m[t.Weekday().String()]
	return t.AddDate(0, 0, day)
}

//是否为移动端
func IsMobile(c *gin.Context) bool {
	agent := c.GetHeader("User-Agent")
	rxp, _ := regexp.Compile("(?i:mobile|nokia|iphone|ipad|android|samsung|htc|blackberry)")
	return rxp.MatchString(agent)
}

//浮点数取小数位数
func FloatFiexd(n float64, l int) float64 {
	d := fmt.Sprintf(fmt.Sprint("%.", l, "f"), n)
	v, _ := strconv.ParseFloat(d, 64)
	return v
}
