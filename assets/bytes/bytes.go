package asset

import (
	"bytes"
	"encoding/binary"
	"strconv"
	"strings"
)

//byte字节相关处理函数

//将byte类型转换成字符串，例如[]byte{1,2,3,4,5}转换成"1,2,3,4,5"
//输入：bt=字节码,dilimiter=连接字符串
func Byte2Str(bt []byte, dilimiter string) string {
	str := ""
	l := len(bt)
	for k, v := range bt {
		if k == l-1 {
			dilimiter = ""
		}
		str = str + strconv.Itoa(int(v)) + dilimiter
	}
	return str
}

//将字符串转换为byte数据
func Str2Byte(str []string) (bt []byte, err error) {
	for _, b := range str {
		nb, e := strconv.Atoi(strings.TrimSpace(b))
		if e != nil {
			return nil, e
		}
		bt = append(bt, byte(nb))
	}
	return bt, nil
}

//16进制字符串转换为byte数据
func Hex2Byte(str []string) (bt []byte, err error) {
	for _, v := range str {
		b, e := strconv.ParseInt(v, 16, 10)
		if e != nil {
			return nil, err
		}
		bt = append(bt, byte(b))
	}
	return bt, nil
}

//将byte转换成数字类型
//输入：b=byte,data=要转换输出的变量'请输入指针
func Byte2Number(b []byte, data interface{}) {
	b_buf := bytes.NewBuffer(b)
	binary.Read(b_buf, binary.BigEndian, data)
	return
}

//数字转换成byte类型
func Number2Byte(data interface{}) []byte {
	b_buf := bytes.NewBuffer([]byte{})
	binary.Write(b_buf, binary.BigEndian, data)
	return b_buf.Bytes()
}
