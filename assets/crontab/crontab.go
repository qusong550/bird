package crontab

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type spec struct {
	interval  bool //是否为定时执行
	any       bool //是否为任意时间可执行
	data      int8 //时间数据
	lastTimer int8 //上一个时间
	timer     int8 //定时器
}

func newSpec(str string, min, max int8, title string) (s *spec, e error) {
	s = &spec{timer: -1}
	if str == "*" {
		s.any = true
		return
	}
	if strings.HasPrefix(str, "/") {
		s.interval = true
		str = str[1:]
	}
	data, e := strconv.ParseInt(str, 10, 8)
	if e != nil {
		return
	}
	s.data = int8(data)
	if s.data < min || s.data > max {
		e = fmt.Errorf("%s参数在%d到%d之间", title, min, max)
	}
	return
}

func (this *spec) match(t int8) bool {
	if this.any {
		return true
	} else if this.interval {
		if this.lastTimer != t {
			this.lastTimer = t
			this.timer++
		}
		//println(s.timer,t,s.data,s.timer-s.data)
		if this.timer-this.data == 0 {
			this.timer = 0
			return true
		}
	} else if this.data > -1 && t == this.data {
		return true
	}
	return false
}

type task struct {
	Month, Day, Weekday  *spec
	Hour, Minute, Second *spec
	Job                  func()
}

//任务集合
var tasks []*task

//spec=月 日 周 小时 分 秒
//* 不限制 / 间隔
//	* 1 * 20 0 0			每月1号20点0分0秒
//	* * * * * /5			每隔5秒执行一次
func NewCron(specs string, job func()) error {
	args := strings.Fields(specs)
	if len(args) != 6 {
		return errors.New("定时任务参数不正确")
	}
	var err error
	var task = &task{Job: job}
	if task.Month, err = newSpec(args[0], 1, 2, "月"); err != nil {
		return err
	}
	if task.Day, err = newSpec(args[1], 1, 31, "日"); err != nil {
		return err
	}
	if task.Weekday, err = newSpec(args[2], 1, 7, "周"); err != nil {
		return err
	}
	if task.Hour, err = newSpec(args[3], 0, 23, "时"); err != nil {
		return err
	}
	if task.Minute, err = newSpec(args[4], 0, 59, "分"); err != nil {
		return err
	}
	if task.Second, err = newSpec(args[5], 0, 59, "秒"); err != nil {
		return err
	}
	tasks = append(tasks, task)
	return nil
}

func (this *task) matches(t time.Time) bool {
	if !this.Month.match(int8(t.Month())) {
		return false
	}
	if !this.Day.match(int8(t.Day())) {
		return false
	}
	if !this.Weekday.match(int8(t.Weekday())) {
		return false
	}
	if !this.Hour.match(int8(t.Hour())) {
		return false
	}
	if !this.Minute.match(int8(t.Minute())) {
		return false
	}
	if !this.Second.match(int8(t.Second())) {
		return false
	}
	return true
}

//
func processTasks() {
	for {
		time.Sleep(time.Second)
		now := time.Now()
		for _, j := range tasks {
			if j.matches(now) && j.Job != nil {
				go j.Job()
			}
		}
	}
}

func init() {
	go processTasks()
}
