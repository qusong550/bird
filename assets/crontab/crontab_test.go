package crontab

import (
	"fmt"
	"testing"
	"time"
)

func TestJob(t *testing.T) {
	err := NewCron("* * * * /2 *", func() {
		fmt.Println("2分钟一次", time.Now().Format("2006-01-02 15:04:05"))
	})
	if err != nil {
		fmt.Println(err.Error())
	}
	err = NewCron("* * * * * /5", func() {
		fmt.Println("17秒一次", time.Now().Format("2006-01-02 15:04:05"))
	})
	if err != nil {
		fmt.Println(err.Error())
	}

	time.Sleep(time.Hour)
}
