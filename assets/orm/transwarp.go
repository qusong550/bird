package orm

//事务包裹
func TransWarp(fn func(*Engine)) {
	eg := new(Engine)
	eg.Transaction(func() {
		fn(eg)
	})
}
