package orm

import (
	"fmt"
	"strings"

	"gitee.com/web-bird/bird/try"
	"xorm.io/xorm"
)

//引擎结构体
type Engine struct {
	parent     Enginer
	db         *xorm.Session
	eg         *xorm.Engine
	beginTrans bool
}

//如果错误致命.
func (this *Engine) ThrowSQL(msg string, data string) {
	sql, args := this.DB().LastSQL()
	argArr := make([]string, 0)
	for _, arg := range args {
		argArr = append(argArr, fmt.Sprint(arg))
	}
	data = fmt.Sprintf("错误：%s,SQL:%s	\n	，参数：%s", data, sql, strings.Join(argArr, ","))
	try.Throw(&ExceptionSQL{}, msg, data)
}

//如果错误致命
func (this *Engine) IfErrorFatal(err error) {
	if err != nil {
		sql, args := this.DB().LastSQL()
		argArr := make([]string, 0)
		for _, arg := range args {
			argArr = append(argArr, fmt.Sprint(arg))
		}
		data := fmt.Sprintf("错误：%s	，\n	SQL:%s	\n	，参数：%s", err.Error(), sql, strings.Join(argArr, ","))
		try.Throw(&ExceptionSQL{}, "数据处理失败", data)
	}
}

//设置引擎
func (this *Engine) SetEngine(eg *xorm.Engine) {
	this.eg = eg
}

//获取引擎
func (this *Engine) GetEngine() *xorm.Engine {
	if this.parent != nil {
		return this.parent.GetEngine()
	}
	if this.eg == nil {
		if engine == nil {
			panic("请先初始化数据库引擎。")
		}
		this.SetEngine(engine)
	}
	return this.eg
}

//获取session
func (this *Engine) DB() *xorm.Session {
	if this.parent != nil {
		return this.parent.DB()
	}
	if this.db == nil {
		this.db = this.GetEngine().NewSession()
	}
	return this.db
}

//继承引擎
func (this *Engine) ExtendEngine(parent Enginer) {
	this.parent = parent
}

//表
func (this *Engine) Table(tableNameOrBean interface{}) *xorm.Session {
	return this.DB().Table(tableNameOrBean)
}

//事务
func (this *Engine) Transaction(call func()) {
	if this.parent != nil {
		this.parent.Transaction(call)
		return
	}
	db := this.DB()
	if this.beginTrans {
		call()
		return
	}
	this.beginTrans = true
	this.IfErrorFatal(db.Begin())
	try.Do(func() {
		call()
		db.Commit()
		this.beginTrans = false
	}, func(e try.Exception) {
		db.Rollback()
		this.beginTrans = false
		try.Throw(e)
	})
}

//初始化模型，并返回模型本身
func (this *Engine) Model(obj ModelInterface) interface{} {
	obj.ExtendEngine(this)
	obj.InitModel(obj)
	return obj
}

//分页查询
//listPtr	= 查询列表指针
//page	= 页码
//limit	= 每页查询数量
func (this *Engine) FindPage(db *xorm.Session, listPtr interface{}, page, limit int) int64 {
	if page <= 0 {
		page = 1
	}
	if limit == 0 {
		limit = 20
	}
	if limit > 1000 {
		limit = 1000
	}
	start := (page - 1) * limit
	total, err := db.Limit(limit, start).FindAndCount(listPtr)
	this.IfErrorFatal(err)
	return total
}

//迭代处理接口
type IteratorHandler interface {
	List() interface{}    //列表生成
	Do(interface{}) error //列表处理
}
