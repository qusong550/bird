package orm

import (
	"fmt"
	"testing"
	"time"

	"xorm.io/core"
	"xorm.io/xorm"
	"xorm.io/xorm/schemas"
)

//参数
type Param struct {
	Name  string      `bind:"name" json:"name"`
	Alias string      `bind:"alias" json:"alias"`
	Value interface{} `bind:"value" json:"value"`
	//Value string `bind:"value1" json:"value1"`
}
type KamiCard struct {
	Number   string `json:"account"`
	Password string `json:"password"`
}
type OrderModel struct {
	Id                 int64
	GoodsId            int64      `xorm:"int(11) index comment('商品编号')"`
	GoodsSnapshotId    int64      `xorm:"int(11) index comment('商品快照id')"`
	OrderSN            string     `xorm:"varchar(20) unique comment('订单序列号')"`
	OrderNum           uint64     `xorm:"int(11) comment('下单数量')"`
	FinishTotal        uint64     `xorm:"int(11) comment('完成总量')"`
	StartNum           uint64     `xorm:"int(11) comment('开始数量')"`
	CurrentNum         uint64     `xorm:"int(11) comment('当前数量')"`
	GoodsPrice         int64      `xorm:"bigint(20) comment('下单单价')"` //必须高于供货价的
	SupplyPrice        int64      `xorm:"bigint(20) comment('供货单价')"`
	OrderAmount        int64      `xorm:"bigint(20) comment('订单总额')"`
	RefundAmount       int64      `xorm:"bigint(20) comment('退款总额')"`
	SupplyAmount       int64      `xorm:"bigint(20) comment('供货总额-供货商查看')"`
	SupplyRefundAmount int64      `xorm:"bigint(20) comment('供货商退款总额-供货商查看')"`
	OrderState         int        `xorm:"tinyint(4) comment('订单状态')"`
	BuyerMemberUserId  int64      `xorm:"int(11) index comment('购买用户id')"`
	Params             []*Param   `xorm:"json comment('下单参数')"`
	ParamsIndex        string     `xorm:"text comment('参数账号索引，查询专用')"` //后期优化
	OrderRemarks       string     `xorm:"text comment('订单处理备注')"`
	ErrorRemarks       string     `xorm:"text comment('错误备注')"`
	OrderNote          string     `xorm:"varchar(255) comment('订单备注')"`
	ExpiredAt          time.Time  `xorm:"datetime comment('到期时间，查询使用')"`
	CreatedAt          time.Time  `xorm:"created comment('创建时间')"`
	UpdatedAt          time.Time  `xorm:"updated comment('修改时间')"`
	OuterOrderSN       string     `xorm:"varchar(40) index comment('外部订单号，串货用')"`
	CustomOrderSN      string     `xorm:"varchar(40) index comment('接口使用的对接单号，避免重复下单')"`
	RechargeId         int64      `xorm:"int(11) comment('充值id，直接付款订单使用')"`
	KamiCards          []KamiCard `xorm:"json comment('卡密数据')"`
	SecKillId          int64      `xorm:"int(11) default(0) comment('秒杀id，0就是非秒杀商品')"`
	Version            int64      `xorm:"version comment('版本')"`
	Model
}

func (this *OrderModel) TableName() string {
	return "Core_Order"
}

func (this *OrderModel) PrimaryKey() interface{} {
	return this.Id
}

func initEngine(table string) *xorm.Engine {
	eg, err := xorm.NewEngine("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", "hait", "hait", "127.0.0.1:3306", table))
	if err != nil {
		fmt.Println(err)
		return nil
	}
	//eg.ShowSQL(true)
	eg.SetMapper(new(core.SameMapper))
	InitEngine(eg)
	return eg
}

func TestDump(t *testing.T) {
	eg := initEngine("bbb")
	//
	tableInfo, _ := eg.TableInfo(new(OrderModel))
	dumpTables := []*schemas.Table{tableInfo}
	Dump("./test/dump.sql", eg, dumpTables)
}

func TestLoad(t *testing.T) {
	eg := initEngine("ccc")
	//
	loadTables := []ModelInterface{new(OrderModel)}
	e := Load("./test/dump.sql", eg, loadTables)
	if e != nil {
		fmt.Println(e.Error())
	}
}

func TestXormDump(t *testing.T) {
	eg := initEngine("bbb")
	dumpTables := make([]*schemas.Table, 0)
	tables, _ := eg.DBMetas()
	for _, table := range tables {
		if table.Name == "core_order" {
			dumpTables = append(dumpTables, table)
		}
	}
	eg.DumpTablesToFile(dumpTables, "./test/dump_xorm.sql")
}

func TestXormLoad(t *testing.T) {
	eg := initEngine("ccc")
	eg.DropTables(new(OrderModel))
	eg.ImportFile("./test/dump_xorm.sql")
}
