package orm

import (
	"gitee.com/web-bird/bird/try"
)

//警告级别错误
type ExceptionSQL struct {
	try.ExceptionBase
}

func (this *ExceptionSQL) Name() string {
	return "数据库异常"
}
