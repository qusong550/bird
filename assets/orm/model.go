package orm

import (
	"fmt"
	"sync"
)

/*------------------  使用示例  -----------------
type User struct {
	Id    int    `xorm:"int(11) autoincr pk" model:""`
	Name  string `xorm:"varchar(32)"`
	Age   int8   `xorm:"int(11)"`
	Model `xorm:"-"`
}

func NewUser() *User {
	u := new(User)
	u.InitModel(u)
	return u
}
注：一定要在结构体的主键字段的tag里声明model,默认主键名与字段名相同，也可任意设置
*/

//模块修改锁，避免出现资源竞争的情况导致死锁
var modelLockMap = sync.Map{}
var modelLock bool = true

//设置模块锁是否开启
func SetModelLock(b bool) {
	modelLock = b
}

type Model struct {
	Engine                       //继承引擎基类
	obj           ModelInterface //model对象
	usePrimarykey bool           //使用默认主键查询
	omit          []string
	cols          []string
	must          []string
	data          interface{}
	cache         bool
}

//初始化数据模型，obj为指针
func (this *Model) InitModel(obj ModelInterface) {
	this.obj = obj
	this.init()
}

//初始化操作
func (this *Model) init() {
	this.omit = make([]string, 0)
	this.cols = make([]string, 0)
	this.must = make([]string, 0)
	this.usePrimarykey = true
	return
}

//表别名
func (this *Model) Alias(alias string) string {
	return fmt.Sprintf("`%s` as `%s`", this.obj.TableName(), alias)
}

//必须有的字段，比如age=0会强制写入数据库
func (this *Model) Must(cols ...string) *Model {
	this.must = append(this.must, cols...)
	return this
}

//忽略添加修改的字段
func (this *Model) Omit(cols ...string) *Model {
	this.omit = append(this.omit, cols...)
	return this
}

//指定添加修改的字段
func (this *Model) Cols(cols ...string) *Model {
	this.cols = append(this.cols, cols...)
	return this
}

//使用条件查询
func (this *Model) Where(query string, args ...interface{}) *Model {
	this.DB().Where(query, args...)
	this.usePrimarykey = false
	return this
}

//对data进行增删改查，优先于this.object
func (this *Model) SetData(data interface{}) *Model {
	this.data = data
	return this
}

//根据指定字段获取对象
func (this *Model) Match(column string, data interface{}) *Model {
	this.DB().And("`"+column+"`=?", data)
	this.usePrimarykey = false
	return this
}

//是否存在
func (this *Model) Exists() bool {
	sx := this.DB().Table(this.obj).Limit(1)
	b, err := sx.NoAutoCondition().Exist(this.object(sx))
	this.IfErrorFatal(err)
	return b
}

//查询数据信息（根据ID）
func (this *Model) Get() bool {
	sx := this.DB().Table(this.obj)
	b, err := sx.NoAutoCondition().Get(this.object(sx))
	this.IfErrorFatal(err)
	return b
}

//添加数据
func (this *Model) Insert() bool {
	sx := this.DB().Table(this.obj)
	l, err := sx.InsertOne(this.object(sx))
	this.IfErrorFatal(err)
	return l == 1
}

//修改数据
func (this *Model) Update() bool {
	if modelLock && this.obj != nil {
		lockKey := fmt.Sprint(this.obj.TableName(), this.obj.PrimaryKey())
		var lockObj *sync.Mutex
		obj, ok := modelLockMap.Load(lockKey)
		if !ok {
			lockObj = &sync.Mutex{}
			modelLockMap.Store(lockKey, lockObj)
		} else {
			lockObj = obj.(*sync.Mutex)
		}
		lockObj.Lock()
		defer lockObj.Unlock()
	}
	sx := this.DB().Limit(1)
	l, err := sx.Update(this.object(sx))
	this.IfErrorFatal(err)
	return l == 1
}

//删除数据（只针对主键id删除）
func (this *Model) Delete() bool {
	sx := this.DB().Limit(1)
	l, err := sx.NoAutoCondition().Delete(this.object(sx))
	this.IfErrorFatal(err)
	return l == 1
}

//获取对象，并进行预处理
func (this *Model) object(sx *Session) (obj interface{}) {
	obj = this.obj
	if this.usePrimarykey {
		sx.ID(this.obj.PrimaryKey())
	}
	if len(this.cols) > 0 {
		sx.Cols(this.cols...)
	}
	if len(this.omit) > 0 {
		sx.Omit(this.omit...)
	}
	if len(this.must) > 0 {
		sx.MustCols(this.must...)
	}
	if this.data != nil {
		obj = this.data
	}
	this.init()
	return obj
}
