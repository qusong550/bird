package orm

import (
	"fmt"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"xorm.io/core"
	"xorm.io/xorm"
)

type User struct {
	Id      int    `xorm:"int(11) autoincr pk" model:""`
	Name    string `xorm:"varchar(32)" put:"user"`
	Age     int64  `xorm:"int(11)" put:"user"`
	Version int    `xorm:"version" put:"sss"`
	Model   `xorm:"-"`
}

func NewUser() *User {
	u := new(User)
	u.InitModel(u)
	return u
}

func (this *User) TableName() string {
	return "User"
}
func (this *User) PrimaryKey() interface{} {
	return this.Id
}

//乐观锁测试
func initdata() {
	eg, err := xorm.NewEngine("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", "root", "", "127.0.0.1:3306", "test"))
	if err != nil {
		fmt.Println(err)
		return
	}
	eg.ShowSQL(true)
	eg.SetMapper(new(core.SameMapper))
	InitEngine(eg)
}

func TestTrans(t *testing.T) {

}
