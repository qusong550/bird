package orm

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"xorm.io/xorm"
	"xorm.io/xorm/schemas"
)

type DataInfo struct {
	Mode      int      `json:"m"` //1=表，2=字段
	TableName string   `json:"t"` //表名（所属表）
	Data      []string `json:"d"` //数据
}

const (
	mode_table = 1
	mode_row   = 2
)

//导出数据库
func Dump(filesrc string, eg *xorm.Engine, tables []*schemas.Table) error {
	f, e := os.OpenFile(filesrc, os.O_CREATE|os.O_RDWR, 0766)
	if e != nil {
		return e
	}
	defer f.Close()
	for _, item := range tables {
		item.Name = strings.ToLower(item.Name)
		var tableInfo = &DataInfo{TableName: item.Name, Mode: mode_table}
		for _, item := range item.Columns() {
			tableInfo.Data = append(tableInfo.Data, item.Name)
		}
		tableData, e := toBase64(tableInfo)
		if e != nil {
			return e
		}
		f.Write(tableData)
		datas, e := eg.Table(item.Name).QueryString()
		if e != nil {
			return e
		}
		for _, data := range datas {
			var rowInfo = &DataInfo{TableName: tableInfo.TableName, Mode: mode_row}
			for _, col := range tableInfo.Data {
				rowInfo.Data = append(rowInfo.Data, data[col])
			}
			tableData, e := toBase64(rowInfo)
			if e != nil {
				return e
			}
			f.Write(tableData)
		}
	}
	return nil
}

func toBase64(v interface{}) ([]byte, error) {
	data, e := json.Marshal(v)
	if e != nil {
		return nil, e
	}
	bod := base64.StdEncoding.EncodeToString(data)
	return []byte(bod + "\n"), nil
}

func toData(val string, ptr interface{}) error {
	data, e := base64.StdEncoding.DecodeString(val)
	if e != nil {
		return e
	}
	if e := json.Unmarshal(data, ptr); e != nil {
		return e
	}
	return nil
}

//加载数据库
func Load(filesrc string, eg *xorm.Engine, tables []ModelInterface) error {
	f, e := os.Open(filesrc)
	if e != nil {
		return e
	}
	tableStructMap := make(map[string]ModelInterface)
	for _, item := range tables {
		tableName := strings.ToLower(item.TableName())
		tableStructMap[tableName] = item
	}
	buff := bufio.NewReader(f)
	tableColsMap := make(map[string][]string)
	tableRealColsMap := make(map[string]map[string]bool)
	for {
		line, err := buff.ReadString('\n')
		if err != nil || io.EOF == err {
			break
		}
		var dataInfo = new(DataInfo)
		if e := toData(line, dataInfo); e != nil {
			return e
		}
		dataInfo.TableName = strings.ToLower(dataInfo.TableName)
		tableObj, ok := tableStructMap[dataInfo.TableName]
		if !ok {
			return fmt.Errorf("缺少表%s的数据结构", dataInfo.TableName)
		}
		switch dataInfo.Mode {
		case mode_table:
			tableColsMap[dataInfo.TableName] = dataInfo.Data
			eg.DropTables(tableObj)
			eg.Sync2(tableObj)
			tableInfo, _ := eg.TableInfo(tableObj)
			tableRealColsMap[dataInfo.TableName] = make(map[string]bool)
			for _, item := range tableInfo.ColumnsSeq() {
				tableRealColsMap[dataInfo.TableName][item] = true
			}
		case mode_row:
			data := make(map[string]interface{})
			colsSlice := tableColsMap[dataInfo.TableName]
			for index, val := range dataInfo.Data {
				colsName := colsSlice[index]
				if val != "" && tableRealColsMap[dataInfo.TableName][colsName] {
					data[colsName] = val
				}
			}
			_, e := eg.Table(tableObj.TableName()).Insert(data)
			if e != nil {
				return e
			}
		default:
			continue
		}
	}
	return nil
}
