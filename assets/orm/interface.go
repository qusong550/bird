package orm

import (
	"xorm.io/xorm"
)

//引擎接口
type Enginer interface {
	SetEngine(*xorm.Engine)
	GetEngine() *xorm.Engine
	DB() *xorm.Session
	Transaction(func())
}

//模型接口
type ModelInterface interface {
	TableName() string        //表名
	PrimaryKey() interface{}  //主键
	ExtendEngine(Enginer)     //继承引擎
	InitModel(ModelInterface) //初始化模型
	Enginer
}
