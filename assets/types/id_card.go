package types

//加密输出身份证号码 例如 510222****1111
type SecretIdCard string

func (self SecretIdCard) MarshalJSON() ([]byte, error) {
	if len(self) != 18 {
		return []byte(`""`), nil
	}
	return []byte(`"` + self[:3] + "****" + self[8:] + `"`), nil
}
