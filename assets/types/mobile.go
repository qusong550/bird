package types

//加密输出手机号码 例如 133****1111
type SecretMobile string

func (self SecretMobile) MarshalJSON() ([]byte, error) {
	if len(self) != 11 {
		return []byte(`""`), nil
	}
	return []byte(`"` + self[:3] + "****" + self[8:] + `"`), nil
}
