package types

//加密输出姓名 例如 *三丰
type SecretName string

func (self SecretName) MarshalJSON() ([]byte, error) {
	d := []rune(self)
	if len(d) <= 1 {
		return []byte(`""`), nil
	}
	return []byte(`"*` + string(d[1:]) + `"`), nil
}
