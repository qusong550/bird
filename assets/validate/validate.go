package validate

//常用绑定验证函数
import (
	"fmt"
	"gitee.com/web-bird/bird/try"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type validate struct {
	data interface{}
	pass bool
}

func Validate(data interface{}) *validate {
	object := new(validate)
	object.data = data
	object.pass = true
	return object
}

//为空验证(对字符、切片、对象类型有效）
func (this *validate) Required() *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case string:
		this.pass = strings.TrimSpace(data) != ""
	case []interface{}:
		this.pass = len(data) > 0
	case nil:
		this.pass = false
	}
	return this
}

//正则式处理
//arg=正则表达式
//tag使用方法 match=^\d+$
//注意：不要在正则式中间加:
func (this *validate) Match(arg string) *validate {
	if this.pass {
		this.pass = this.match(arg)
	}
	return this
}

func (this *validate) match(arg string) bool {
	r := regexp.MustCompile(arg)
	return r.MatchString(fmt.Sprint(this.data))
}

//长度验证(对数字、字符、切片类型有效)
//tag使用方法 len=10
func (this *validate) Len(l int) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int, int8, int16, int32, int64, uint, uint8, uint32, uint64, float32, float64, string:
		this.pass = len([]rune(fmt.Sprint(data))) == l
	case []interface{}:
		this.pass = len(data) == l
	}
	return this
}

//等于(对数字、字符、布尔类型有效）
//tag使用方法 eq=10
func (this *validate) Eq(arg string) *validate {
	if this.pass {
		this.pass = fmt.Sprint(this.data) == arg
	}
	return this
}

//在范围内(对数字、字符、布尔类型有效）
//tag使用方法 in=a|b|c，多个规则使用|分隔
func (this *validate) In(datas ...interface{}) *validate {
	if !this.pass {
		return this
	}
	this.pass = false
	for _, v := range datas {
		if v == this.data {
			this.pass = true
			break
		}
	}
	return this
}

//不在范围内(对数字、字符、布尔类型有效）
//tag使用方法 not-in=a|b|c，多个规则使用|分隔
func (this *validate) NotIn(datas ...interface{}) *validate {
	if this.pass {
		return this
	}
	this.pass = true
	for _, v := range datas {
		if this.data == v {
			this.pass = false
			break
		}
	}
	return this
}

//大于(对数字类型有效）
//tag使用方法 gt=10
func (this *validate) Gt(a float64) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int:
		this.pass = int64(data) > int64(a)
	case int8:
		this.pass = int64(data) > int64(a)
	case int16:
		this.pass = int64(data) > int64(a)
	case int32:
		this.pass = int64(data) > int64(a)
	case int64:
		this.pass = int64(data) > int64(a)
	case uint:
		this.pass = int64(data) > int64(a)
	case uint8:
		this.pass = int64(data) > int64(a)
	case uint32:
		this.pass = int64(data) > int64(a)
	case uint64:
		this.pass = int64(data) > int64(a)
	case float32:
		this.pass = data-float32(a) > 0
	case float64:
		this.pass = data-a > 0
	}
	return this
}

//大于等于(对数字类型有效）
//tag使用方法 gte=10
func (this *validate) Gte(a float64) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int:
		this.pass = int64(data) >= int64(a)
	case int8:
		this.pass = int64(data) >= int64(a)
	case int16:
		this.pass = int64(data) >= int64(a)
	case int32:
		this.pass = int64(data) >= int64(a)
	case int64:
		this.pass = int64(data) >= int64(a)
	case uint:
		this.pass = int64(data) >= int64(a)
	case uint8:
		this.pass = int64(data) >= int64(a)
	case uint32:
		this.pass = int64(data) >= int64(a)
	case uint64:
		this.pass = int64(data) >= int64(a)
	case float32:
		this.pass = data-float32(a) >= 0
	case float64:
		this.pass = data-a >= 0
	}
	return this
}

//小于(对数字类型有效）
//tag使用方法 lt=10
func (this *validate) Lt(a float64) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int:
		this.pass = int64(data) < int64(a)
	case int8:
		this.pass = int64(data) < int64(a)
	case int16:
		this.pass = int64(data) < int64(a)
	case int32:
		this.pass = int64(data) < int64(a)
	case int64:
		this.pass = int64(data) < int64(a)
	case uint:
		this.pass = int64(data) < int64(a)
	case uint8:
		this.pass = int64(data) < int64(a)
	case uint32:
		this.pass = int64(data) < int64(a)
	case uint64:
		this.pass = int64(data) < int64(a)
	case float32:
		this.pass = data-float32(a) < 0
	case float64:
		this.pass = data-a < 0
	}
	return this
}

//小于等于(对数字类型有效）
//tag使用方法 lte=10
func (this *validate) Lte(a float64) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int:
		this.pass = int64(data) <= int64(a)
	case int8:
		this.pass = int64(data) <= int64(a)
	case int16:
		this.pass = int64(data) <= int64(a)
	case int32:
		this.pass = int64(data) <= int64(a)
	case int64:
		this.pass = int64(data) <= int64(a)
	case uint:
		this.pass = int64(data) <= int64(a)
	case uint8:
		this.pass = int64(data) <= int64(a)
	case uint32:
		this.pass = int64(data) <= int64(a)
	case uint64:
		this.pass = int64(data) <= int64(a)
	case float32:
		this.pass = data-float32(a) <= 0
	case float64:
		this.pass = data-a <= 0
	}
	return this
}

//最小长度(对字符、数字、切片类型有效）
//tag使用方法 min=10
func (this *validate) Min(l int) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int, int8, int16, int32, int64, uint, uint8, uint32, uint64, float32, float64, string:
		this.pass = len([]rune(fmt.Sprint(data))) >= l
	case []interface{}:
		this.pass = len(data) >= l
	}
	return this
}

//最大长度度(对字符、数字、切片类型有效）
//tag使用方法 max=10
func (this *validate) Max(l int) *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case int, int8, int16, int32, int64, uint, uint8, uint32, uint64, float32, float64, string:
		this.pass = len([]rune(fmt.Sprint(data))) <= l
	case []interface{}:
		this.pass = len(data) <= l
	}
	return this
}

//日期 格式 2006-01-02
//tag使用方法 date
func (this *validate) Date() *validate {
	if !this.pass {
		return this
	}
	switch data := this.data.(type) {
	case string:
		_, err := time.Parse("2006-01-02", data)
		this.pass = err == nil
	}
	return this
}

//日期时间 格式 2006-01-02 15:04:05
//tag使用方法 datetime
func (this *validate) Datetime(val interface{}) *validate {
	if !this.pass {
		return this
	}
	switch data := val.(type) {
	case string:
		_, err := time.Parse("2006-01-02 15:04:05", data)
		this.pass = err == nil
	}
	return this
}

//时间
func (this *validate) HourMinute(val interface{}) *validate {
	if !this.pass {
		return this
	}
	switch data := val.(type) {
	case string:
		_, err := time.Parse("15:04", data)
		this.pass = err == nil
	}
	return this
}

/*
 * 验证所给手机号码是否符合手机号的格式.
 * 移动: 134、135、136、137、138、139、150、151、152、157、158、159、182、183、184、187、188、178(4G)、147(上网卡)；
 * 联通: 130、131、132、155、156、185、186、176(4G)、145(上网卡)、175；
 * 电信: 133、153、180、181、189 、177(4G)；
 * 卫星通信:  1349
 * 虚拟运营商: 170、171,173
 * 2018新增: 16x, 19x
 */
//tag使用方法 mobile
func (this *validate) Mobile() *validate {
	return this.Match(`^1[3,4,5,6,7,8,9][\d]{9}$`)
	//return IsMatch(val, `^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^16[\d]{9}$|^17[0,1,3,5,6,7,8]{1}\d{8}$|^18[\d]{9}$|^19[\d]{9}$`)
}

//国内座机电话号码："XXXX-XXXXXXX"、"XXXX-XXXXXXXX"、"XXX-XXXXXXX"、"XXX-XXXXXXXX"、"XXXXXXX"、"XXXXXXXX"
//tag使用方法 tel
func (this *validate) Tel() *validate {
	return this.Match(`^((\d{3,4})|\d{3,4}-)?\d{7,8}$`)
}

//腾讯QQ号，从10000开始
//tag使用方法 tel
func (this *validate) QQ(val interface{}) *validate {
	return this.Match(`^[1-9][0-9]{4,}$`)
}

//邮政编码
//tag使用方法 postcode
func (this *validate) Postcode() *validate {
	return this.Match(`^\d{6}$`)
}

//帐号规则(字母开头，只能包含字母、数字和下划线，长度在6~18之间)
//tag使用方法 account
func (this *validate) Account() *validate {
	return this.Match(`^\w{5,17}$`)
}

//通用密码(任意可见字符，长度在6~18之间)
//tag使用方法 password
func (this *validate) Password() *validate {
	return this.Match(`^[\w\S]{6,18}$`)
}

//中等强度密码(在弱密码的基础上，必须包含大小写字母和数字)
//tag使用方法 password2
func (this *validate) Password2() *validate {
	if !this.pass {
		return this
	}
	this.pass = this.match(`^[\w\S]{6,18}$`) &&
		this.match(`[a-z]+`) &&
		this.match(`[A-Z]+`) &&
		this.match(`\d+`)
	return this
}

// 强等强度密码(在弱密码的基础上，必须包含大小写字母、数字和特殊字符)
//tag使用方法 password3
func (this *validate) Password3(val interface{}) *validate {
	if !this.pass {
		return this
	}
	this.pass = this.match(`^[\w\S]{6,18}$`) &&
		this.match(`[a-z]+`) &&
		this.match(`[A-Z]+`) &&
		this.match(`\d+`) &&
		this.match(`[^a-zA-Z0-9]+`)
	return this
}

//邮件
//tag使用方法 email
func (this *validate) Email() *validate {
	return this.Match(`^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+)+$`)
}

//URL
//tag使用方法 url
func (this *validate) URL() *validate {
	return this.Match(`^(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]`)
}

//domain
//tag使用方法 domain
func (this *validate) Domain() *validate {
	return this.Match(`^([0-9a-zA-Z][0-9a-zA-Z-]{0,62}\.)+([0-9a-zA-Z][0-9a-zA-Z-]{0,62})\.?$`)
}

//MAC地址
//tag使用方法 mac
func (this *validate) Mac() *validate {
	return this.Match(`^([0-9A-Fa-f]{2}[\-:]){5}[0-9A-Fa-f]{2}$`)
}

//IP4地址
//tag使用方法 ip4
func (this *validate) IP4() *validate {
	return this.Match(`^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$`)
}

//IP6地址
//tag使用方法 ip6
func (this *validate) IP6() *validate {
	return this.Match(`^([\da-fA-F]{1,4}:){7}[\da-fA-F]{1,4}$|^:((:[\da-fA-F]{1,4}){1,6}|:)$|^[\da-fA-F]{1,4}:((:[\da-fA-F]{1,4}){1,5}|:)$|^([\da-fA-F]{1,4}:){2}((:[\da-fA-F]{1,4}){1,4}|:)$|^([\da-fA-F]{1,4}:){3}((:[\da-fA-F]{1,4}){1,3}|:)$|^([\da-fA-F]{1,4}:){4}((:[\da-fA-F]{1,4}){1,2}|:)$|^([\da-fA-F]{1,4}:){5}:([\da-fA-F]{1,4})?$|^([\da-fA-F]{1,4}:){6}:$`)
}

//IP地址(ip4和ip6均可）
//tag使用方法 ip
func (this *validate) IP() *validate {
	if !this.pass {
		return this
	}
	this.pass = this.match(`^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$`) &&
		this.match(`^([\da-fA-F]{1,4}:){7}[\da-fA-F]{1,4}$|^:((:[\da-fA-F]{1,4}){1,6}|:)$|^[\da-fA-F]{1,4}:((:[\da-fA-F]{1,4}){1,5}|:)$|^([\da-fA-F]{1,4}:){2}((:[\da-fA-F]{1,4}){1,4}|:)$|^([\da-fA-F]{1,4}:){3}((:[\da-fA-F]{1,4}){1,3}|:)$|^([\da-fA-F]{1,4}:){4}((:[\da-fA-F]{1,4}){1,2}|:)$|^([\da-fA-F]{1,4}:){5}:([\da-fA-F]{1,4})?$|^([\da-fA-F]{1,4}:){6}:$`)
	return this
}

/*
   公民身份证号
   xxxxxx yyyy MM dd 375 0     十八位
   xxxxxx   yy MM dd  75 0     十五位

   地区：[1-9]\d{5}
   年的前两位：(18|19|([23]\d))      1800-2399
   年的后两位：\d{2}
   月份：((0[1-9])|(10|11|12))
   天数：(([0-2][1-9])|10|20|30|31) 闰年不能禁止29+

   三位顺序码：\d{3}
   两位顺序码：\d{2}
   校验码：   [0-9Xx]

   十八位：^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$
   十五位：^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$

   总：
   (^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)
*/
//tag使用方法 id-number
func (this *validate) IDNumber() *validate {
	if !this.pass {
		return this
	}
	this.pass = false
	value, ok := this.data.(string)
	if !ok {
		return this
	}
	value = strings.ToUpper(strings.TrimSpace(value))
	// 18位长检测
	if len(value) != 18 {
		return this
	}
	// 加权因子
	weightFactor := [...]int{7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2}
	// 校验码
	checkCode := [...]byte{'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'}
	last := value[17]
	num := 0
	for i := 0; i < 17; i++ {
		tmp, err := strconv.Atoi(string(value[i]))
		if err != nil {
			return this
		}
		num = num + tmp*weightFactor[i]
	}
	resisue := num % 11
	if checkCode[resisue] != last {
		return this
	}
	return this.Match(`(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)`)
}

//LUHN规则(银行卡号验证规则)
//tag使用方法 luhn
func (this *validate) LuHn() *validate {
	if !this.pass {
		return this
	}
	this.pass = false
	value, ok := this.data.(string)
	if !ok {
		return this
	}
	var sum = 0
	var nDigits = len(value)
	var parity = nDigits % 2
	for i := 0; i < nDigits; i++ {
		var digit = int(value[i] - 48)
		if i%2 == parity {
			digit *= 2
			if digit > 9 {
				digit -= 9
			}
		}
		sum += digit
	}
	this.pass = sum%10 == 0
	return this
}

//数组是否有重复
func (this *validate) ArrayNotRepeat() *validate {
	if !this.pass {
		return this
	}
	this.pass = false
	switch data := this.data.(type) {
	case []string:
		var m = make(map[string]bool)
		for _, item := range data {
			if m[item] {
				return this
			}
			m[item] = true
		}
	}
	return this
}

//是否有错误，直接抛出异常
func (this *validate) IsErr(errMsg string) {
	if !this.pass {
		try.ThrowInfo(errMsg)
	}
}

func (this *validate) IsPass() bool {
	return this.pass
}
