package validate

import (
	"fmt"
	"gitee.com/web-bird/bird/try"
	"testing"
)

func TestValidate(t *testing.T) {
	var username = "王海涛"
	var age = 50
	try.Do(func() {
		Validate(username).Len(2).IsErr("用户名错误")
		Validate(age).Gt(1).Lt(100).IsErr("年龄在10-100之间")
	}, func(e try.Exception) {
		fmt.Println(e.GetMsg())
	})
}
