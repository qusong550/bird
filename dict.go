package bird

import (
	"fmt"

	"gitee.com/web-bird/bird/try"
)

/*
	数据字典
*/

//字典
type Dict struct {
	Alias      string      `json:"alias"`      //字典别名
	Name       string      `json:"name"`       //字典名称
	ModuleName string      `json:"moduleName"` //字典所在模块名
	Items      []*DictItem `json:"items"`      //字典
}

//字典列表
type DictItem struct {
	Const string      `json:"const"` //常量名
	Label string      `json:"label"` //名称
	Value interface{} `json:"value"` //值
	Desc  string      `json:"desc"`  //描述
}

//获取label名
func (this *Dict) Label(value interface{}) string {
	for _, item := range this.Items {
		if value == item.Value {
			return item.Label
		}
	}
	return ""
}

//添加元素
func (this *Dict) AddItem(constName string, label string, value interface{}, desc string) *Dict {
	for _, item := range this.Items {
		if label == item.Label {
			try.ThrowFatal(fmt.Sprintf("字典【%s】名【%s】重复", this.Name, label))
		}
		if value == item.Value {
			try.ThrowFatal(fmt.Sprintf("字典【%s】值【%s】重复", this.Name, label))
		}
	}
	this.Items = append(this.Items, &DictItem{constName, label, value, desc})
	return this
}

//检查元素是有重复的
func (this *Dict) checkItems() {
	labels := make(map[string]bool)
	values := make(map[interface{}]bool)
	for _, item := range this.Items {
		if labels[item.Label] {
			try.ThrowFatal(fmt.Sprintf("字典【%s】名【%s】重复", this.Name, item.Label))
		}
		labels[item.Label] = true
		if values[item.Value] {
			try.ThrowFatal(fmt.Sprintf("字典%s值%s重复", this.Name, item.Label))
		}
		values[item.Value] = true
	}
}
