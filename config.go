package bird

//核心配置
type conf struct {
	Debug      bool   //是否启动调试
	CloseQueue bool   //是否关闭队列
	MD5        string //sha1加盐使用
	ServerAddr string //启动地址
	CertFile   string //秘匙
	KeyFile    string //公匙
	Database   struct {
		Type   string //数据库类型
		Source string //数据库地址
	}
	JWT struct {
		Issuer   string
		Secret   string
		Duration int //有效时间，单位分
	}
}

var config conf

func GetConfig() conf {
	return config
}
