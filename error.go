package bird

import (
	"gitee.com/web-bird/bird/try"
)

//数据绑定错误
type ExceptionBind struct {
	Field string
	try.ExceptionBase
}

func (this *ExceptionBind) Name() string {
	return "数据验证"
}

//权限错误
type ExceptionPermission struct {
	try.ExceptionBase
}

func (this *ExceptionPermission) Name() string {
	return "权限"
}

//jwt异常【未登录】
type ExceptionJWT struct {
	try.ExceptionBase
}

func (this *ExceptionJWT) Name() string {
	return "JWT登录"
}
