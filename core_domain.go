package bird

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"gitee.com/web-bird/bird/assets/orm"
	"gitee.com/web-bird/bird/assets/utils"
	"gitee.com/web-bird/bird/try"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"xorm.io/core"
	"xorm.io/xorm"
)

//保证系统只执行一次入口程序
var once sync.Once

type coreDomain struct {
	app     AppInterface
	modules []ModuleInterface
	server  *gin.Engine
}

//
var _coreDomain *coreDomain

func CoreDomain() *coreDomain {
	if _coreDomain == nil {
		object := new(coreDomain)
		_coreDomain = object
	}
	return _coreDomain
}

//启动应用
//请使用bird/app.Run启动
func (this *coreDomain) Start(app AppInterface) {
	if this.app != nil {
		log.Println("不可重复启动")
		return
	}
	if _, ok := app.(fromBirder); !ok {
		log.Fatal("请使用web-bird/bird/app包的Run方法启动")
	}
	once.Do(func() {
		this.app = app
		this.modules = app.Modules()
		//初始化目录
		this.initDir()
		//加载核心配置
		ConfigDomain().loadCoreConfig()
		//初始化数据库
		this.initDatabase()
		//有限执行执行debug调试
		if fn := this.app.Debug(); fn != nil {
			fn()
			return
		}
		//启动web服务
		this.initServer()
	})
}

//所有模块
func (this *coreDomain) Modules() []ModuleInterface {
	return this.modules
}

//初始化目录
func (this *coreDomain) initDir() {
	if err := os.MkdirAll(CONFIG_DIR, 0700); err != nil {
		try.ThrowFatal("无法创建config目录")
	}
	if err := os.MkdirAll(RUNTIME_DIR, 0700); err != nil {
		try.ThrowFatal("无法创建runtime目录")
	}
}

var OrmEngine *xorm.Engine

//初始化数据库,并进行安装尝试
func (this *coreDomain) initDatabase() {
	//初始化引擎
	engine, err := xorm.NewEngine(config.Database.Type, config.Database.Source)
	if err != nil {
		try.ThrowFatal("数据库初始失败", err.Error())
		return
	}
	engine.SetMaxOpenConns(500)
	engine.SetMapper(new(core.SameMapper))
	engine.StoreEngine("InnoDB")
	engine.Charset("utf8mb4")
	engine.SetTZLocation(time.Local)
	if config.Debug {
		engine.ShowSQL(true)
	} else {
		engine.ShowSQL(false)
	}
	if err := utils.TestSqlConnect(config.Database.Type, config.Database.Source); err != nil {
		try.ThrowFatal("数据库连接失败：", err.Error())
	}
	OrmEngine = engine
	orm.InitEngine(engine)
}

func (this *coreDomain) install() {
	//安装数据
	var installLockFile = RUNTIME_DIR + "/install.lock"
	_, err := ioutil.ReadFile(installLockFile)
	if err == nil {
		return
	}
	//调用app的install方法
	bird := NewBird()
	ModuleDomain().install(this.modules)
	this.app.Install(bird)
	data := time.Now().Format("2006-01-02 15:04:05")
	if err := ioutil.WriteFile(installLockFile, []byte(data), 0600); err != nil {
		try.ThrowFatal("安装锁定失败：", err.Error())
	}
}

//初始化gin引擎
func (this *coreDomain) initServer() {
	//设置gin运行模式
	if config.Debug {
		gin.SetMode("debug")
	} else {
		gin.SetMode("release")
	}
	this.server = gin.New()
	if config.Debug {
		this.server.Use(gin.Logger())
	}
	//异常捕捉，此处可以用来做日志处理等
	this.server.Use(func(c *gin.Context) {
		try.Do(func() {
			c.Next()
		}, func(e try.Exception) {
			w := NewContext(c)
			code := ExceptionDomain().handler(w.Bird, e)
			w.Msg(code, e.GetMsg())
			c.Abort()
		})
	})
	//应用初始化
	this.app.Init(this.server)
	//
	this.install()
	//模块初始化
	ModuleDomain().init(this.modules)
	//异常处理初始化
	ExceptionDomain().init()
	var err error
	if strings.TrimSpace(config.KeyFile) != "" && strings.TrimSpace(config.CertFile) != "" {
		err = this.server.RunTLS(config.ServerAddr, config.CertFile, config.KeyFile)
	} else {
		err = this.server.Run(config.ServerAddr)
	}
	if err != nil {
		try.ThrowFatal("网络服务启动失败", err.Error())
	}
}
