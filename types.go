package bird

//任意类型
type X interface{}

//hash
type H map[string]X

//查询结构
type BaseQuery struct {
	Page  int `bind:"page" form:"page"`
	Limit int `bind:"limit" form:"limit"`
}
