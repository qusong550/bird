package bird

import (
	"encoding/json"

	"gitee.com/web-bird/bird/try"
)

/*
	队列任务时异步且有序执行的，遵循先进先出原则
*/

//队列获任务获取
var taskStore func(b *Bird) TaskStoreInterface

//设置队列任务获取器
func SetQueueTaskStore(t func(b *Bird) TaskStoreInterface) {
	taskStore = t
}

//队列任务
type Task struct {
	QueueId    int64  //队列id
	QueueAlias string //队列别名
	Arg        string //参数
	Err        string //错误
}

//反序列化参数，ptr=参数指针
func (this *Task) GetArgument(ptr interface{}) {
	err := json.Unmarshal([]byte(this.Arg), ptr)
	if err != nil {
		try.ThrowFatalf("队列任务参数获取错误", "任务名：%s，错误：%s", this.QueueAlias, err.Error())
	}
}
